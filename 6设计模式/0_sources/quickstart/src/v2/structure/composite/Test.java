package v2.structure.composite;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Test {

    public static void main(String[] args) throws Exception {

        Node node1 = new Node("node1", 0);
        Node node11 = new Node("node11", 1);
        Node node12 = new Node("node12", 1);
        node1.add(node11);
        node1.add(node12);

        Node leaf1 = new Node("leaf1", 2);
        Node leaf2 = new Node("leaf2", 2);
        Node leaf3 = new Node("leaf3", 2);
        Node leaf4 = new Node("leaf4", 2);

        Node leaf5 = new Node("leaf5", 2);

        node11.add(leaf1);
        node11.add(leaf2);
        node12.add(leaf3);
        node12.add(leaf4);
        node1.add(leaf5);

        node1.display();
        node1.remove(leaf5);
        node1.display();
    }
}
