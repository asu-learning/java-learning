package v2.structure.composite;

/**
 * 节点数据，可以再添加 id, parentId, leafed 是否叶节点，可按需求扩展
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public abstract class NodeData {
    private String name;
    private String fullName;
    private Integer level;

    public NodeData(String name, Integer level) {
        this.name = name;
        this.fullName = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public abstract void display();
}
