package v2.structure.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合模式允许您将对象组合成树形结构以表示“整体-部分”的层次关系。
 * 这种模式创建了对象组的树形结构，以便您可以将对象组合成树状结构以表示部分以及整体层次
 *
 * 文件系统中的文件夹可以包含其他文件夹和文件，而文件夹和文件都可以被视为节点。
 *
 * 使用节点数据NodeData和Node来表示即可
 * NodeData存放节点属性数据， Node为节点容器，包含节点数据和节点列表。
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Node extends NodeData {
    private List<NodeData> children;

    public Node(String name, Integer level) {
        super(name, level);
        children = new ArrayList<>();
    }

    public void add(NodeData child) throws Exception {
        if (get(child.getName()) == null){
            child.setFullName(this.getFullName()+"/"+child.getName());
            children.add(child);
        } else {
            throw new Exception("child already exists");
        };
    }

    public void remove(NodeData child) {
        children.remove(child);
    }

    public NodeData get(Integer index){
        return children.get(index);
    }
    public NodeData get(String name){
        for (NodeData child : children) {
            if (name.equalsIgnoreCase(child.getName())){
                return child;
            }
        }
        return null;
    }

    @Override
    public void display() {
        System.out.println("Node " + getFullName());
        for (NodeData child : children) {
            child.display();
        }
    }
}
