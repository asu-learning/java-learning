package v2.structure.facade;

/**
 * 提供电脑门面类，将cpu，dist方法分别封装进来，以简化客户端的调用
 *
 * 外观模式也可以叫做门面模式，它提供了一个简单的接口来访问一个复杂的子系统，使得子系统更容易使用。
 * 主要目的是降低系统的复杂性，同时提高客户端与子系统的交互性。
 *
 * 一般通过创建一个外观类来实现，该外观类集成了子系统的多个类，
 * 并将其他类的操作都委托给子系统来完成，最终封装一个接口给客户端使用
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class ComputerFacade {
    private Cpu cpu;
    private Disk disk;

    public ComputerFacade() {
        cpu = new Cpu();
        disk = new Disk();
    }

    public void start() {
        this.cpu.start();
        this.disk.start();
    }

    public void shutDown() {
        this.cpu.shutDown();
        this.disk.shutDown();
    }
}
