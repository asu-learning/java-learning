package v2.structure.facade;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Test {
    public static void main(String[] args) {
        ComputerFacade facade = new ComputerFacade();
        facade.start();

        facade.shutDown();
    }
}
