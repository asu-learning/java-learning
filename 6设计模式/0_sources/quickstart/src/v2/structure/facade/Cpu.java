package v2.structure.facade;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Cpu {
    public void start() {
        System.out.println("cpu is start...");
    }

    public void shutDown() {
        System.out.println("CPU is shutDown...");
    }
}
