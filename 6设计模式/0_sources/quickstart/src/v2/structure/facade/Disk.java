package v2.structure.facade;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Disk {
    public void start() {
        System.out.println("Disk is start...");
    }

    public void shutDown() {
        System.out.println("Disk is shutDown...");
    }
}
