package v2.structure.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元工厂（FlyweightFactory）:负责管理享元对象池和创建享元对象
 *
 * 用于管理对象池，初始化个数， 闲置时个数，最大个数等等
 * intrinsicState表示内部状态，用于标识对象
 *
 * 享元模式又叫轻量级模式，既是共享元件模型，是对象池的一种标签，
 * 将多个同一对象的访问集中起来，避免不停的创建和销毁对象，消耗性能，类似线程池，数据库连接池。
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class FlyweightFactory {
    public static Map<String, Flyweight> pool = new HashMap<>();

    public static Flyweight getFlyweight(String intrinsicState){
        if (pool.containsKey(intrinsicState)){
            return pool.get(intrinsicState);
        }else{
            ConcreteFlyweight concreteFlyweight = new ConcreteFlyweight(intrinsicState);
            pool.put(intrinsicState, concreteFlyweight);
            return concreteFlyweight;
        }
    }
}
