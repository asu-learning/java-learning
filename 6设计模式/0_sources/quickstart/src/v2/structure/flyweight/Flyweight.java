package v2.structure.flyweight;

/**
 * 抽象享元角色， 接口，或抽象类
 *
 * extrinsicState：外部状态，可以被客户端调用
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public interface Flyweight {
    void operation(String extrinsicState);
}
