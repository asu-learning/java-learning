package v2.structure.flyweight;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Test {
    public static void main(String[] args) {
        FlyweightFactory factory = new FlyweightFactory();
        Flyweight circle = FlyweightFactory.getFlyweight("circle");
        circle.operation("圆形");

        Flyweight rectangle = FlyweightFactory.getFlyweight("rectangle");
        rectangle.operation("方形");

    }
}
