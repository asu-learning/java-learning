package v2.structure.flyweight;

/**
 * 具体享元角色（ConcreteFlyweight）： 具体的业务实体，实现方法逻辑
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class ConcreteFlyweight implements Flyweight{
    //内部状态
    private String intrinsicState;

    public ConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }

    @Override
    public void operation(String extrinsicState) {
        System.out.println("extrinsicState:"+extrinsicState);
        System.out.println("intrinsicState:"+intrinsicState);
    }
}
