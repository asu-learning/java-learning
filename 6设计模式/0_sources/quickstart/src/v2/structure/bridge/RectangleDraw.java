package v2.structure.bridge;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class RectangleDraw extends BridgeDraw {
    public RectangleDraw(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        System.out.println("rectangle draw");
    }
}
