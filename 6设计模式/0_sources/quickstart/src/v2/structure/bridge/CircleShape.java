package v2.structure.bridge;

/**
 * 圆形
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class CircleShape extends Shape{
    //半径
    private double radius;

    public CircleShape(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2* Math.PI * radius;
    }
}
