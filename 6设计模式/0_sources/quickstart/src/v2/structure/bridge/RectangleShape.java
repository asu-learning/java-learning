package v2.structure.bridge;

/**
 * 长方形
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class RectangleShape extends Shape {
    private double width;
    private double height;

    public RectangleShape(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double getArea() {
        return width*height;
    }

    @Override
    public double getPerimeter() {
        return 2*(width+height);
    }
}
