package v2.structure.bridge;

/**
 * 定义业务的抽象类或接口
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public abstract class Shape {
    //面积
    public abstract double getArea();
    //周长
    public abstract double getPerimeter();
}
