package v2.structure.bridge;

/**
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public class Test {
    public static void main(String[] args) {
        CircleDraw circle = new CircleDraw(new CircleShape(5));
        circle.draw();
        System.out.println(circle.getShape().getArea());

        RectangleDraw rect = new RectangleDraw(new RectangleShape(2,4));
        rect.draw();
        System.out.println(rect.getShape().getArea());

    }
}
