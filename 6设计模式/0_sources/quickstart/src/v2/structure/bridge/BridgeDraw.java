package v2.structure.bridge;

/**
 * 基于桥接模式，定义桥接画图形的抽象类
 *
 * 将画图和图形桥接起来，比如画长方形，圆形， 将实现部分放在子类中
 *
 * 桥接抽象类，也可以是接口， 也即使说Shape也可以定义为接口
 *
 * @author : zhenyun.su
 * @since : 2023/10/13
 */

public abstract class BridgeDraw {
    private Shape shape;

    public BridgeDraw(Shape shape) {
        this.shape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    public abstract void draw();
}
