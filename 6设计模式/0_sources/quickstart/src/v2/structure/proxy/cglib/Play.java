package v2.structure.proxy.cglib;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public interface Play {
    String look(String value);
}
