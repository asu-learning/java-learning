package v2.structure.proxy.cglib;

import v2.structure.proxy.jdk.Buy;
import v2.structure.proxy.jdk.BuySaleHouse;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class Test {
    public static void main(String[] args) {
        //场景1， 基于普通对象(类无接口)，进行代理， 最简单Cglib代理来实现
        House baseHouse = new House();
        House proxy = (House)SimpleCglibProxy.getProxyObject(baseHouse);
        proxy.buy1();
        // buy2为final方法， 未被代理
        proxy.buy2();


        //场景2， 基于类接口进行代理， 基于抽象代理类的子类来代理，子类可以为不同产品织入不同代码逻辑
        Buy buy = new BuySaleHouse();
        HouseCglibProxy houseProxy = new HouseCglibProxy(buy);
        BuySaleHouse proxy2 = (BuySaleHouse)houseProxy.getProxyObject();
        proxy2.buy1();
        proxy2.sale();

        BuyMobile buyMobile = new BuyMobile();
        MobileCglibProxy mobileCglibProxy = new MobileCglibProxy(buyMobile);
        Play proxy3 = (BuyMobile)mobileCglibProxy.getProxyObject();
        proxy3.look("flower");

    }
}
