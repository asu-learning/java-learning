package v2.structure.proxy.cglib;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class HouseCglibProxy extends BaseCglibProxy {

    public HouseCglibProxy(Object target) {
        super(target);
    }

    @Override
    public void before(Object target, Object[] args, String methodNme) {
        System.out.println("买房前准备");
    }

    @Override
    public void after(Object target, Object[] args, String methodNme, Object result) {
        System.out.println("买房后装修");
    }
}
