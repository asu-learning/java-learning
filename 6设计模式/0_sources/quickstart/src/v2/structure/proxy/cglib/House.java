package v2.structure.proxy.cglib;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class House {

    public void buy1() {
        System.out.println("House-购房1，先支付房款后签合同");
    }

    public final void buy2() {
        System.out.println("House-购房2，先签合同后支付房款");
    }

}
