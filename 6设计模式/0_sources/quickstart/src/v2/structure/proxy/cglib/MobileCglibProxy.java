package v2.structure.proxy.cglib;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class MobileCglibProxy extends BaseCglibProxy {

    public MobileCglibProxy(Object target) {
        super(target);
    }

    @Override
    public void before(Object target, Object[] args,  String methodNme) {
        System.out.println(methodNme+": 买手机前准备");
        for (Object arg : args) {
            System.out.println("parameter:" + arg);
        }
    }

    @Override
    public void after(Object target, Object[] args, String methodNme, Object result) {
        System.out.println(methodNme+": 安装APP");
        if (result != null){
            System.out.println("result:"+result);
        }
    }
}
