package v2.structure.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 基于cglib-3.3.0.jar和asm-7.1.jar实现，cglib依赖于asm实现
 *
 * 最简单Cglib代理类实现，可为target业务类的所有public方法，提供代理，除了final的方法
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class SimpleCglibProxy  {
    public static Object getProxyObject(Object target){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("买房前准备");
                Object result = methodProxy.invoke(target, objects);
                System.out.println("买房后装修");
                return result;
            }
        });
        return enhancer.create();
    }
}
