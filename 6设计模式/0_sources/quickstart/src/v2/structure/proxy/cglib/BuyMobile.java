package v2.structure.proxy.cglib;

import v2.structure.proxy.jdk.Buy;

/**
 * 买房业务类
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class BuyMobile implements Buy, Play {

    @Override
    public void buy1() {
        System.out.println("购买手机1");
    }

    @Override
    public void buy2() {
        System.out.println("购买手机1");
    }

    @Override
    public String look(String value) {
        return "look-"+value;
    }
}
