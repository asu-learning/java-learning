package v2.structure.proxy.jdk;

/**
 * 买房业务类
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class BuySaleHouse implements Buy, Sale {

    @Override
    public void buy1() {
        System.out.println("购房1，先支付房款后签合同");
    }

    @Override
    public void buy2() {
        System.out.println("购房2，先签合同后支付房款");
    }


    @Override
    public void sale() {
        System.out.println("卖房子");
    }
}
