package v2.structure.proxy.jdk;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public interface Sale {
    void sale();
}
