package v2.structure.proxy.define;

/**
 * 买房业务类
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class BuyHouse implements Buy {

    @Override
    public void buy() {
        System.out.println("支付房款和签合同");
    }
}
