package v2.structure.proxy.define;

/**
 * 代理BuyHouse类来实现
 *
 * 这种方式只适用于购买房子， 我们可以基于接口来实现更加灵活的代理,
 * 参考实现接口buy的抽象代理类ProxyBuy
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class SimpleProxyBuyHouse implements Buy {
    private BuyHouse buyHouse;

    public SimpleProxyBuyHouse(BuyHouse buyHouse) {
        this.buyHouse = buyHouse;
    }
    @Override
    public void buy() {
        System.out.println("选房，沟通");
        this.buyHouse.buy();
        System.out.println("办理贷款");
    }
}
