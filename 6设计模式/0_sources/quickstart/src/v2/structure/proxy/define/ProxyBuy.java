package v2.structure.proxy.define;

/**
 *
 * 抽象接口方法的抽象代理类， 可以代理任何实现接口buy的业务类，比如ProxyBuyMobile
 *
 * 购买不同东西，购买的流程不一样，因此通过抽象方法buy()来实现接口Buy
 *
 * 在子类中根据购买不同东西特性来实现增强逻辑。
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public abstract class ProxyBuy implements Buy {
    protected Buy buy;

    public ProxyBuy(Buy buy) {
        this.buy = buy;
    }

    @Override
    public abstract void buy();
}
