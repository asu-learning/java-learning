package v2.structure.proxy.define;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public interface Buy {
    void buy();
}
