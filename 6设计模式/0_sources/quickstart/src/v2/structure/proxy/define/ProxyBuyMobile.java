package v2.structure.proxy.define;

/**
 * 购买手机代理类，在buy实现购买手机的增强逻辑，并调用业务类
 *
 * 基于ProxyBuy来实现购买不同东西的代理类，比如汽车，电脑等
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class ProxyBuyMobile extends ProxyBuy{
    public ProxyBuyMobile(Buy buy) {
        super(buy);
    }

    @Override
    public void buy() {
        System.out.println("推荐手机");
        this.buy.buy();
        System.out.println("安装APP");
    }
}
