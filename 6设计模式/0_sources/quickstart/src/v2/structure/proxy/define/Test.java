package v2.structure.proxy.define;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class Test {
    public static void main(String[] args) {
        //直接注入对象，同时实现buy接口的简单代理类
        BuyHouse b = new BuyHouse();
        SimpleProxyBuyHouse sp = new SimpleProxyBuyHouse(b);
        sp.buy();

        //通过手机代理类，来代理，new Buy的购买华为手机的实现类
        ProxyBuyMobile p = new ProxyBuyMobile(new Buy() {
            @Override
            public void buy() {
                System.out.println("购买华为手机");
            }
        });
        p.buy();
    }
}
