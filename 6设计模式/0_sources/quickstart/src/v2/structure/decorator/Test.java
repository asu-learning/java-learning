package v2.structure.decorator;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 *
 * 打印结果：
 * normalNoodle: desc=noodle, price=8.00
 * BeefNoodle: desc=noodle&&beaf, price=28.00
 * CarrotNoodle: desc=noodle&&carrot, price=13.00
 * CarrotNoodle: desc=noodle&&beaf&&carrot, price=33.00
 */

public class Test {
    public static void main(String[] args) {

        // //制作光面
        // NormalNoodle noodle = new NormalNoodle();
        // System.out.println(noodle.print());
        // //添加牛肉的面
        // BeefNoodle beefNoodle = new BeefNoodle(noodle);
        // System.out.println(beefNoodle.print());
        //
        // //添加胡萝卜的面
        // CarrotNoodle carrotNoodle = new CarrotNoodle(noodle);
        // System.out.println(carrotNoodle.print());
        //
        // //再添加胡萝卜的面
        // CarrotNoodle carrotBeefNoodle = new CarrotNoodle(beefNoodle);
        // System.out.println(carrotBeefNoodle.print());

        //加入ConcreteComponent是一碗面条，基础价格为8块
        ConcreteComponent component = new ConcreteComponent(8f);
        System.out.println(component.getCost());
        //价格+8.0

        // 加牛肉20块
        BeefDecorator beefDecorator = new BeefDecorator(component);
        System.out.println(beefDecorator.getCost());
        //价格+28.0

        // 牛肉基础上再加胡萝卜5块
        CarrotDecorator carrotDecorator = new CarrotDecorator(beefDecorator);
        System.out.println(carrotDecorator.getCost());
        //最终价格+33.0


    }
}
