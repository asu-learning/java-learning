package v2.structure.adapter;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class Mac implements TypeCable {
    @Override
    public void typeC() {
        System.out.println("使用Type-C接口");
    }
}
