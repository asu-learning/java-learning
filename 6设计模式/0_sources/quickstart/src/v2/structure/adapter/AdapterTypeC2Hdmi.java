package v2.structure.adapter;

/**
 * 对象适配器方式： 通过继承MacTypeC类，并实现Hdmi方法来适配
 *
 * 本代码是注入MacTypeC类，推荐注入方法TypeCable根据通用
 *
 * 若适配器，针对一类对象有特殊逻辑需要处理，那将AdapterTypeC2Hdmi改为抽象类，在有子类来执行个性适配逻辑
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class AdapterTypeC2Hdmi implements Hdmiable {
    //修改TypeCable接口，更通用
    private Mac macTypeC;

    public AdapterTypeC2Hdmi(Mac macTypeC) {
        this.macTypeC = macTypeC;
    }

    @Override
    public void hdmi() {
        System.out.println("基于对象适配模式，执行hdmi接口时，适配typeC接口");
        // do to adapter statement
        macTypeC.typeC();
    }
}
