package v2.structure.adapter;

/**
 * 类适配器方式： 通过继承MacTypeC类，并实现Hdmi方法来适配
 *
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class AdapterMac2Hdmi extends Mac implements Hdmiable {
    @Override
    public void hdmi() {
        System.out.println("基于类适配模式，执行hdmi接口时，适配typeC接口");
        // do to adapter statement
        super.typeC();
    }
}
