package v2.structure.adapter;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public class Test {
    public static void main(String[] args) {

        AdapterMac2Hdmi adapterMac2Hdmi = new AdapterMac2Hdmi();
        adapterMac2Hdmi.hdmi();

        Mac mac = new Mac();
        AdapterTypeC2Hdmi adapterTypeC2hmi = new AdapterTypeC2Hdmi(mac);
        adapterTypeC2hmi.hdmi();
    }


}
