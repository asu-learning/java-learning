package v2.structure.adapter;

/**
 * @author : zhenyun.su
 * @since : 2023/10/12
 */

public interface Hdmiable {
    void hdmi();
}
