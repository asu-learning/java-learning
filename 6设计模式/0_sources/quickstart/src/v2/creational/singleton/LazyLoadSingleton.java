package v2.creational.singleton;

/**
 * @author : zhenyun.su
 * @comment : 单实例 - 懒加载模式 在需要使用对象时，才被创建
 *  关键说明： 最佳实践，推荐使用
 *  基本原理：
 *  1. 并指定instance变量为volatile, 保证实例对象放在主内存区，而非线程的工作区，保证所有线程共享实例对象
 *  2. 基于"双重检查锁定"（Double-Checked Locking）模式，
 *      第一次检查instance是否为null，是在已经创建实例下，无需进行同步操作，提高性能。
 *      第二次检查在同步锁下判断instance是否为null，在多线程情况下，会存在多个线程(instance为空)的都在等待同步锁时，
 *          保证在实例已经创建的情况下，避免重复创建新的实例
 *      synchronized (LazyLoadSingleton.class)
 *          用来获取LazyLoadSingleton类的锁的。这意味着只有一个线程可以进入synchronized代码块，
 *          其他线程必须等待这个线程执行完synchronized代码块
 *  使用场景：适合所有场景，和预加载模式相比，性能慢点可忽略不计。 超高并发的化那就用预加载模式
 *
 * @since : 2019/8/19
 */

public class LazyLoadSingleton {
    public static volatile LazyLoadSingleton instance = null;

    private LazyLoadSingleton() {
    }

    public static  LazyLoadSingleton getInstance(){
        if (instance == null){
            synchronized (LazyLoadSingleton.class){
                if (instance == null){
                    instance= new LazyLoadSingleton();
                }
            }
        }
        return instance;
    }
}
