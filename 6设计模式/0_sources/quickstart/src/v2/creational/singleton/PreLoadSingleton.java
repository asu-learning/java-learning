package v2.creational.singleton;

/**
 * @author : zhenyun.su
 * @comment : 单实例 - 预加载模式， 在程序启动时也就时类加载时，就被创建
 * 基本原理： 在类加载时就创建实例对象，缺点没在使用前就已经使用内存， 优点可保证线程安全，而且代码简单
 * 适用场景：在这个应用系统中，单实例对象很少。并且使用很频繁
 * @since : 2019/8/19
 */

public class PreLoadSingleton {
    public static PreLoadSingleton instance = new PreLoadSingleton();

    private PreLoadSingleton() {
    }

    public static PreLoadSingleton getInstance(){
        return instance;
    }
}
