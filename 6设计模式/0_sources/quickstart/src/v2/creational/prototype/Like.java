package v2.creational.prototype;

/**
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Like implements Cloneable {
    private String music;

    public Like(String music) {
        this.music = music;
    }

    @Override
    public Like clone() throws CloneNotSupportedException {
        return (Like)super.clone();
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    @Override
    public String toString() {
        return "Like{" +
                "music='" + music + '\'' +
                '}';
    }
}
