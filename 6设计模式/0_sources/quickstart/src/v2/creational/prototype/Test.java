package v2.creational.prototype;

/**
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Test {

    public static void main(String[] args) throws Exception {
        Like like = new Like("rain");
        System.out.println("person1:"+like.clone());

        Person oldPerson = new Person("person", "su", 20, like);

        //深拷贝
        Person newPerson = oldPerson.clone();
        // 修改旧person的like属性
        oldPerson.getLike().setMusic("summer");
        System.out.println("old person:"+oldPerson);

        //发现拷贝like属性，没有随着oldPerson修改而修改，说是深度拷贝
        System.out.println("new person:"+newPerson);
    }
}
