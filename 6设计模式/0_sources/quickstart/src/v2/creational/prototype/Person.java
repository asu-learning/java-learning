package v2.creational.prototype;

/**
 * 实现Cloneable接口，提供clone方法，以实现深拷贝功能
 *
 * 用于体现原型模式
 *
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Person implements Cloneable {
    private String type;
    private String name;
    private int salary;
    private Like like;

    public Person() {
    }

    public Person(String type, String name, int salary, Like like) {
        this.type = type;
        this.name = name;
        this.salary = salary;
        this.like = like;
    }

    // 覆载父类方法Object方法
    @Override
    public Person clone() throws CloneNotSupportedException {
        //浅拷贝，复制所有基本属性
        Person p = (Person) super.clone();
        //深拷贝，克隆引用类型后再复制给like
        p.like = this.like.clone();
        return p;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Like getLike() {
        return like;
    }

    public void setLike(Like like) {
        this.like = like;
    }

    @Override
    public String toString() {
        return "Person{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", like=" + like +
                '}';
    }
}
