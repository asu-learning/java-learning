package v2.creational.builder.simple;

/**
 * 一个最简单构建器模式实例
 *
 * 在Person类，内置一个Person构建器的静态类PersonBuilder， 用来封装构建Person实例
 * lombok的@Builder注解，在编译后生产代码也使用构建器设计模型
 *
 * 使用方法
 * Person person = Person.buider().name("su").type("simple").age(30).build();
 *
 * 当然也可以将内部类PersonBuilder单独拿出来作为实现更加复杂的对象构建器， 请参考下
 *
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Person {
    private String type;
    private String name;
    private int salary;

    public Person() {
    }

    public Person(String type, String name, int salary) {
        this.type = type;
        this.name = name;
        this.salary = salary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    public static PersonBuilder buider(){
        return new PersonBuilder();
    }

    public static class PersonBuilder {
        private String type;
        private String name;
        private int salary;

        public PersonBuilder type(String type) {
            this.type = type;
            return this;
        }

        public PersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder salary(Integer salary) {
            this.salary = salary;
            return this;
        }

        public static final String dd = "ddd";

        public Person build() {
            return new Person(this.type, name, salary);
        }

        @Override
        public String toString() {
            return "PersonBuilder{" +
                    "type='" + type + '\'' +
                    ", name='" + name + '\'' +
                    ", salary=" + salary +
                    '}';
        }
    }
}

