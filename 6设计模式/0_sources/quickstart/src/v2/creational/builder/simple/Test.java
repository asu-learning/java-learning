package v2.creational.builder.simple;

import base.clazz.Person;

/**
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Test {

    public static void main(String[] args) {
        v2.creational.builder.simple.Person person = v2.creational.builder.simple.Person.buider().name("su").type("simple").salary(30).build();
        System.out.println(person);
        System.out.println(v2.creational.builder.simple.Person.PersonBuilder.dd);

    }
}
