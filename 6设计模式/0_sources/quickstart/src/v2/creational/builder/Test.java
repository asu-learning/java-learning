package v2.creational.builder;

/**
 *
 *
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Test {

    public static void main(String[] args) {
        EmployeePersonBuilder employeePersonBuilder = new EmployeePersonBuilder();
        System.out.println(employeePersonBuilder.build("su1", 20));

        CustomerPersonBuilder customerPersonBuilder = new CustomerPersonBuilder();
        System.out.println(customerPersonBuilder.build("su2", 20));
    }
}
