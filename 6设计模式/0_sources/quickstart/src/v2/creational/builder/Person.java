package v2.creational.builder;

/** 员工
 *
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class Person {
    private String type;
    private String name;
    private int salary;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }
}
