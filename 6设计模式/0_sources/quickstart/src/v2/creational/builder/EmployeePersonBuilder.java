package v2.creational.builder;

/**
 * 对象具体生成器，用于封装构建对象及封装对象组装逻辑， 这里员工人员生成器
 *
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public class EmployeePersonBuilder extends PersonBuilder {
    @Override
    public void setField(String name, Integer salary) {
        this.person.setType("employee");
        this.person.setName(name);
        this.person.setSalary(salary+2000);
    }
}
