package v2.creational.builder;

/**
 * 人员抽象生成器，提供抽象实例化对象，提供抽象方法，封装对象组装逻辑
 *
 * 也可以将整个对象组装封装在抽象生成器中，而不是指挥者类中，不过这样
 * @author : zhenyun.su
 * @since : 2023/10/11
 */

public abstract class PersonBuilder {
    protected Person person = null;

    public Person build(String name, Integer salary){
        person = new Person() ;
        this.setField(name, salary);
        return person;
    };

    public abstract void setField(String name, Integer salary);
}
