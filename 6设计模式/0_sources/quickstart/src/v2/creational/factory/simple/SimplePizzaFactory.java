package v2.creational.factory.simple;

/**
 * @author : zhenyun.su
 * @comment : 简单工厂类模式下的工厂类
 * 1. 根据不同披萨名称，生产不同披萨，默认奶酪披萨
 * @since : 2019/8/19
 */

public class SimplePizzaFactory {

    public static Pizza createPizza(String name){
        if (name.equals("cheese")){
            return new CheesePizza();
        }else if (name.equals("clam")){
            return new ClamPizza();
        }else {
            return new CheesePizza();
        }
    }
}
