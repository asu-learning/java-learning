package v2.creational.factory.simple;

/**
 * @author : zhenyun.su
 * @comment : 披萨店， 调用工厂类以获取披萨
 * @since : 2019/8/19
 */

public class PizzaStore {

    // 订购披萨
    public Pizza orderPizza(String name){
        System.out.println("order "+name+ " pizza");
        Pizza pizza = SimplePizzaFactory.createPizza(name);
        pizza.prepare();
        pizza.make();
        pizza.box();
        return pizza;
    }
}
