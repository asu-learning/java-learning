package v2.creational.factory.simple;

/**
 * @author : zhenyun.su
 * @comment :
 * @since : 2019/8/19
 */

public abstract class Pizza {

    public void prepare(){
        System.out.println("准备原材料");
    }

    public void make(){
        System.out.println("制作披萨");
    }

    public void box(){
        System.out.println("打包披萨");
    }

}
