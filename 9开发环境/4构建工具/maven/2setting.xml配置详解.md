## setting.xml配置详解

settings配置官网地址： https://maven.apache.org/settings.html 

将官方setting.xml 拷贝setting_defualt.xml

以下 3.8.1版本的配置

maven安装成功后，settings.xml 存放两个地方

1. 当前用户： ${user.home}/.m2/settings.xml.
2. 全局用户： ${maven.conf}/settings.xml.   适用所有用户的配置

## 配置说明

setting所有元素如下

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository/>
  <interactiveMode/>
  <offline/>
  <pluginGroups/>
  <servers/>
  <mirrors/>
  <proxies/>
  <profiles/>
  <activeProfiles/>
</settings>
```

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository>${user.home}/.m2/repository</localRepository>   <!--本地仓库地址--> 
  <interactiveMode>true</interactiveMode>                          <!--交互模式-->  
  <offline>false</offline>                                         <!--离线模式，只能从本地获取，默认true--> 
  ...
  
  <pluginGroups>                                                   <!--插件组-->     
    <pluginGroup>org.eclipse.jetty</pluginGroup>      
  </pluginGroups>                     <!--默认自动包含 org.apache.maven.plugins， org.codehaus.mojo.--> 
  
  <servers>                          <!--下载jar包 根据repositories，上传部署jar 根据distributionManagement-->                                                   	                                    <!--服务器，认证配置，只配置在settings，无需在pom.xml中配置敏感信息-->   
    <server>                         <!--也可用于配置tomcat管理员的用户名和密码-->                                
      <id>server001</id>                                           
      <username>my_login</username>
      <password>my_password</password>
      <privateKey>${user.home}/.ssh/id_dsa</privateKey>
      <passphrase>some_passphrase</passphrase>
      <filePermissions>664</filePermissions>
      <directoryPermissions>775</directoryPermissions>
      <configuration></configuration>
    </server>
  </servers>  
  
  <mirrors>                  <!--配置可使用maven镜像仓库-->      
    <mirror>
      <id>planetmirror.com</id>           
      <name>PlanetMirror Australia</name>
      <url>http://downloads.planetmirror.com/pub/maven2</url>
      <mirrorOf>central</mirrorOf>  <!--通过这个来查找repositories的id，进而用镜像url来代替repositories的地址-->   
    </mirror>
  </mirrors>
  
  <proxies>                   <!--配置代理网络-->  
    <proxy>
      <id>myproxy</id>
      <active>true</active>
      <protocol>http</protocol>
      <host>proxy.somewhere.com</host>
      <port>8080</port>
      <username>proxyuser</username>
      <password>somepassword</password>
      <nonProxyHosts>*.google.com|ibiblio.org</nonProxyHosts>
    </proxy>
  </proxies>
  
  <profiles>             <!--配置多个环境的仓库-->      
    <profile>
	    <id>nexus-ribo</id>
      <repositories>
        <repository>
          <id>codehausSnapshots</id>
          <name>Codehaus Snapshots</name>
          <releases>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
          </releases>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
            <checksumPolicy>fail</checksumPolicy>
          </snapshots>
          <url>http://snapshots.maven.codehaus.org/maven2</url>
          <layout>default</layout>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>myPluginRepo</id>
          <name>My Plugins repo</name>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <url>https://maven-central-eu....com/maven2/</url>
        </pluginRepository>
      </pluginRepositories>
      ...
    </profile>
    <profile>
	    <id>nexus-aliyun</id>
      <repositories>  
         ...       
      </repositories>  
      </profile>  
    
  </profiles> 
  
  <activeProfiles>     <!--按profiles的id来激活仓库-->   
    <activeProfile>nexus-aliyun</activeProfile>
  </activeProfiles>
</settings>
```



## 最佳实践



```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 http://maven.apache.org/xsd/settings-1.2.0.xsd">

  <localRepository>/Users/asushiye/Programming/java/environment/maven/mavenRepo</localRepository>
   <!--<localRepository>${user.home}/.m2</localRepository>-->
  
  <interactiveMode>true</interactiveMode>
  <offline>false</offline>

  <!--插件组默认自动包含： org.apache.maven.plugins， org.codehaus.mojo.--> 
  <pluginGroups></pluginGroups>
  <proxies></proxies>

  <!--用于repositories，distributionManagement，及tomcat服务器--> 
  <servers>
    <server>
      <id>ribo-server</id>
      <username>admin</username>
      <password>123456</password>
    </server>
  </servers>

  <mirrors></mirrors>

  <profiles>
      <!-- ribo私服 -->
  	  <profile>
	    <id>nexus-ribo</id>
	    <repositories>
	      <repository>
	        <id>ribo-server</id>
	        <url>http://10.10.2.92:8085/repository/maven-public/</url>
	        <releases>
	          <enabled>true</enabled>
	        </releases>
	        <snapshots>
	          <enabled>true</enabled>
	        </snapshots>
	      </repository>	    
	    </repositories>
	    <pluginRepositories>
	      <pluginRepository>
	        <id>ribo-server</id>
	        <url>http://10.10.2.92:8085/repository/maven-public/</url>
	        <releases>
	          <enabled>true</enabled>
	        </releases>
	        <snapshots>
	          <enabled>true</enabled>
	        </snapshots>
	      </pluginRepository>
	    </pluginRepositories>
	  </profile>
      <!-- 阿里云仓库 -->
	  <profile>
		  <id>nexus-aliyun</id>
		  <repositories>
			  <repository>
				  <id>aliyun</id>
				  <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
				  <releases>
					  <enabled>true</enabled>
				  </releases>
				  <snapshots>
					  <enabled>true</enabled>
				  </snapshots>
			  </repository>
		  </repositories>
		  <pluginRepositories>
			  <pluginRepository>
				  <id>aliyun</id>
				  <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
				  <releases>
					  <enabled>true</enabled>
				  </releases>
				  <snapshots>
					  <enabled>true</enabled>
				  </snapshots>
			  </pluginRepository>
		  </pluginRepositories>
	  </profile>
      <!-- maven中央仓库 -->
	  <profile>
	    <id>nexus-central</id>
	    <repositories>
	      <repository>
	        <id>central</id>
	        <url>https://repo1.maven.org/maven2/</url>
	        <releases>
	          <enabled>true</enabled>
	        </releases>
	        <snapshots>
	          <enabled>true</enabled>
	        </snapshots>
	      </repository>
	    </repositories>
	    <pluginRepositories>
	      <pluginRepository>
	        <id>central</id>
	        <url>https://repo1.maven.org/maven2/</url>
	        <releases>
	          <enabled>true</enabled>
	        </releases>
	        <snapshots>
	          <enabled>true</enabled>
	        </snapshots>
	      </pluginRepository>
	    </pluginRepositories>
	  </profile>
  </profiles>

  <activeProfiles>
    <activeProfile>nexus-ribo</activeProfile>
    <activeProfile>nexus-aliyun</activeProfile>
    <activeProfile>nexus-central</activeProfile>
  </activeProfiles>

</settings>
```

上面ribo-server 私服，需要安装nexus私服并配置管理和权限后才能使用

该配置可以做为模板使用，更多配置说明，请参考默认setting.xml配置注释说明

