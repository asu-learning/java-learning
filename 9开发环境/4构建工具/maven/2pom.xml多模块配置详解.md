# 5项目maven配置
    3多模块配置
      依赖关系
      聚合关系
      继承关系
    4最佳实践

## 3多模块配置

为了更好管理代码和代码复用，实现项目松耦合，模块化，往往将项目分解为多个子项目。那么maven如何管理子项目的呢？

maven为项目的依赖关系，聚合关系，继承关系提供相应的功能来解决。

### 实例如下

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!-- 继承关系 -->
    <parent>
        <groupId>com.af.study</groupId>
        <artifactId>projectA</artifactId>
        <version>2.2.7</version>
        <relativePath/> <!-- 可忽略，表示在projectB上级目录查找projectA的pom.xml文件 -->
    </parent>
    <groupId>com.af.study</groupId>
    <artifactId>projectB</artifactId>
    <version>2.2.7</version>
    <packaging>pom</packaging>
    <name>projectB</name>
    <!-- 聚合关系 -->
    <modules>
        <module>1-projectB-core</module>
        <module>2-projectB-data/21-jpa-mysql</module>
	</modules>

    <!-- 依赖关系 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>af.tool</groupId>
                <artifactId>af-dependencies</artifactId>
                <version>${af-tool.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
    </dependencyManagement>
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
```
目录结构如下：
```
--projectA
  --pom.xml
  --projectB
    --pom.xml 
    --1-projectB-core
       --pom.xml 
    --2-projectB-data 
       --21-jpa-mysql
       --pom.xml 
```

### 继承关系
子项目继承父项目也就是继承父项目pom.xml文件
好处呢： 任何的子项目都可以共享父项目定义依赖， 配置，插件等资源

下面基于projectB继承projectA来说明

projectB目录在projectA目录下的配置
```xml
<project >
    <modelVersion>4.0.0</modelVersion>
    <!-- 继承关系 -->
    <parent>
        <groupId>com.af.study</groupId>
        <artifactId>projectA</artifactId>
        <version>2.2.7</version>
    </parent>

    <groupId>com.af.study</groupId>
    <artifactId>projectB</artifactId>
    <version>2.2.7</version>
</project>
```
若projectB和projectA在统一目录下呢
```xml
<project >
    <modelVersion>4.0.0</modelVersion>
    <!-- 继承关系 -->
    <parent>
        <groupId>com.af.study</groupId>
        <artifactId>projectA</artifactId>
        <version>2.2.7</version>
        <relativePath>../projectA/pom.xml</relativePath>
    </parent>

    <groupId>com.af.study</groupId>
    <artifactId>projectB</artifactId>
    <version>2.2.7</version>
</project>
```

### 聚合关系
聚合有利于统一进行编译，打包，部署等操作

1-projectB-core和2-projectB-data/21-jpa-mysql被聚合到projectB下，
因此也称1-projectB-core和2-projectB-data/21-jpa-mysql为projectB的子模块

通常的目录结构如下
```
  --projectB
    --pom.xml 
    --1-projectB-core
       --pom.xml 
    --2-projectB-data 
       --21-jpa-mysql
       --pom.xml
```
配置如下       
```xml
<project >
    <groupId>com.af.study</groupId>
    <artifactId>projectB</artifactId>
    <version>2.2.7</version>
    
    <modules>
        <module>1-projectB-core</module>
        <module>2-projectB-data/21-jpa-mysql</module>
	</modules>
</project>
```

若projectB和1-projectB-core及1-projectB-data同级目录，如何配置
```xml
<project >
    <groupId>com.af.study</groupId>
    <artifactId>projectB</artifactId>
    <version>2.2.7</version>
    
    <modules>
        <module>../1-projectB-core</module>
        <module>../2-projectB-data/21-jpa-mysql</module>
	</modules>
</project>
```

关键点： 
1. module 填写目录，而不是artifactId
2. 子模块可以不用设置version， 默认自动使用父项目的version，除非要特别指定。
3. 利用maven的versions插件，统一设置父项目及所有子Module的parent的版本号

### maven的versions插件
若子模块太多，设置每个模块的父项目依赖版本号，非常麻烦，maven3提供versions插件来完成
```sh
#为父项目及所有子Module的parent的版本号都改成了2.2.8， 当前在maven3.8.1版本下验证 
#执行完成会每个模块都会生成pom.xml.versionsBackup的备份文件
mvn versions:set -DnewVersion=2.2.8  

mvn versions:update-child-modules   
# 提交自动删除所有备份文件
mvn versions:commit
```
子模块之间如果相互依赖，可以在父项目中配置版本号变量，然后在子模块中引用即可。

### 依赖关系



## 4最佳实践

为了更好引用多个子模块的项目，推荐创建一个用于管理所有子模块的依赖项目，比如af-tool-dependencies 子模块

该子模块下的，所有依赖，不能使用父项目的配置版本， 只能固定指定版本，否则其他项目无法引用af-tool-dependencies下管理的依赖。

参考：af-tool 项目

https://gitee.com/asu-java/af-tool.git
