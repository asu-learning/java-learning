# HotKey   ------    快捷键

		文件操作
		光标操作
		代码操作
		注释操作
		窗口操作
		构建操作

## window and mac
https://blog.csdn.net/laioujiao5773/article/details/120655458

操作(英文)	操作(中文)	Windows快捷键	MacOS快捷键
Remember these Shortcuts
Smart code completion	智能代码补全	Ctrl + Shift + Space	⌃⇧Space
Search everywhere	快速查找	Double Shift	Double ⇧
Show intention actions and quick-fixes	显示意图行动和快速修正	Alt + Enter	⌘↩
Generate code	生成代码	Alt + Ins	⌘N,⌃↩
Introduce Variable	接收返回值	Alt + Ins	⌥⌘V
Parameter info	参数信息	Ctrl + P	⌘P
Extend selection	扩展选择	Ctrl + W	⌥⌘UP
Shrink selection	缩小选择	Ctrl + Shift + W	⌥⌘DOWN
Recent files popup	最近文件弹出	Ctrl + E	⌘E
Rename	重命名	Shift + F6	⇧F6
General
Open corresponding tool window	打开相应的工具窗口	Alt + #[0-9]	⌘0..⌘9
Save all	保存所有	Ctrl + S	⌘S
Synchronize	同步	Ctrl + Alt + Y	⌘⌥Y
Toggle maximizing editor	最大化切换编辑器	Ctrl + Shift + F12	⌘⇧F12
Inspect current file with current profile	检查当前文件与当前概要文件*	Alt + Shift + I	⌘⇧I
Quick switch current scheme	快速切换当前计划	Ctrl + BackQuote (`)	⌃§,⌃`
Open Settings dialog	打开设置对话框	Ctrl + Alt + S	⌘,
Open Project Structure dialog	打开项目结构对话框	Ctrl + Alt + Shift + S	⌘;
Find Action	查找操作	Ctrl + Shift + A	⌘⇧A
Debugging
Step over / into	下一步/进入	F8 / F7	F8/F7
Smart step into/Step out	智能进入/跳出	Shift + F7 / Shift + F8	⇧F7/⇧F8
Run to cursor	运行到光标	Alt + F9	⌥F9
Evaluate expression	计算表达式	Alt + F8	⌥F8
Resume program	恢复程序	F9	⌘⌥R
Toggle breakpoint	切换断点	Ctrl + F8	⌘F8
View breakpoints	查看断点	Ctrl + Shift + F8	⌘⇧F8
Search / Replace
Search everywhere	快速查找	Double Shift	Double⇧
Find	文件内查找	Ctrl + F	⌘F
Find next /previous	查找下一个/上一个	F3 / Shift + F3	⌘G/⌘⇧G
Replace	替换	Ctrl + R	⌘R
Find in path	在路径内查找	Ctrl + Shift + F	⌘⇧F
Replace in path	在路径内替换	Ctrl + Shift + R	⌘⇧R
Select next occurrence	选择下个出现的文本	Alt + J	⌃G
Select all occurrences	选择全部选中的文本	Ctrl + Alt + Shift + J	⌃⌘G
Unselect occurrence	取消选择下个出现的文本	Alt + Shift + J	⌃G
Editing
Basic code completion	基本代码补全	Ctrl + Space	⌃Space
Smart code completion	智能代码补全	Ctrl + Shift + Space	⌃⇧Space
Complete statement	补全语句	Ctrl + Shift + Enter	⌘⇧↩
Parameter info (within method call arguments)	在方法调用参数信息(参数)	Ctrl + P	⌘P
Quick documentation lookup	快速查找文档	Ctrl + Q	⌃J,
External Doc	外部文档	Shift + F1	⇧F1
Brief Info	概览	Ctrl + mouse	⌘+mouse
Show descriptions of error at caret	在插入符号显示错误描述	Ctrl + F1	⌘F1
Generate code...	代码生成	Alt + Insert	⌘N,⌃↩
Override methods	覆写方法	Ctrl + O	⌃O
Implement methods	实现方法	Ctrl + I	⌃I
Surround with… 	用…环绕代码	Ctrl + Alt + T	⌘⌥T
Comment /uncomment with line comment	注释/取消注释 行注释	Ctrl + /	⌘/
Comment /uncomment with block comment	注释/取消注释 块注释	Ctrl + Shift + /	⌘⌥/
Extend selection	扩展选择	Ctrl + W	⌥UP
Shrink selection	缩小选择	Ctrl + Shift + W	⌥DOWN
Context info	上下文信息	Alt + Q	⌃⇧Q
Show intention actions and quick-fixes	显示意图行动和快速修正	Alt + Enter	⌥↩
Reformat code	格式化代码	Ctrl + Alt + L	⌘⌥L
optimize imports	优化导包	Ctrl + Alt + O	⌃⌥O
Auto-indent line(s)	自行缩进 行	Ctrl + Alt + I	⌃⌥L
Indent /unindent selected lines	缩进/取消缩进	Tab / Shift + Tab	tab/⇧tab
Cut current line to clipboard	剪切当前行到剪切板	Ctrl + X , Shift + Delete	⌘X
Copy current line to clipboard	复制当前行到剪切板	Ctrl + C , Ctrl + Insert	⌘C
Paste from clipboard	从剪切板粘贴	Ctrl + V , Shift + Insert	⌘V
Paste from recent buffers...	从剪切板选择粘贴	Ctrl + Shift + V	⌘⇧V
Duplicate current line	复制当前行	Ctrl + D	⌘D
Delete line at caret	删除当前行	Ctrl + Y	⌘BACKSPACE
Smart line join	智能合并行	Ctrl + Shift + J	⌃⇧J
Smart line split	智能线分裂	Ctrl + Enter	⌘↩
Start new line	开始新的一行	Shift + Enter	⇧↩
Toggle case for word at caret or selected block	切换大小写	Ctrl + Shift + U	⌘⇧U
Select till code block end / start	选择到代码块结束/开始	Ctrl + Shift + ] / [	⌘⇧]/⌘⇧[
Delete to word end	删除到单词结束	Ctrl + Delete	⌥DEL
Delete to word start	删除到单词开始	Ctrl + Backspace	⌥BACKSPACE
Expand/collapse code block	展开/折叠代码块	Ctrl + NumPad+ / -	⌘+/⌘-
Expand all	全部展开	Ctrl + Shift + NumPad+	⌘⇧+
Collapse all	全部折叠	Ctrl + Shift + NumPadClose	⌘⇧-
active editor tab	关闭当前选项卡	Ctrl + F4	⌘W
Refactoring
Copy	复制	F5	F5
Move	移动	F6	F6
Safe Delete	安全删除	Alt + Delete	⌘DEL
Rename	重命名	Shift + F6	⇧F6
Refactor this	重构	Ctrl + Alt + Shift + T	⌃T
Change Signature	更改签名	Ctrl + F6	⌘F6
Inline	内联	Ctrl + Alt + N	⌘⌥N
Extract Method	提取方法	Ctrl + Alt + M	⌘⌥M
Extract Variable	提取变量	Ctrl + Alt + V	⌘⌥V
Extract Field	提取字段	Ctrl + Alt + F	⌘⌥F
Extract Constant	提取常量	Ctrl + Alt + C	⌘⌥C
Extract Parameter	提取参数	Ctrl + Alt + P	⌘⌥P
Navigation
Go to class	查找class	Ctrl + N	⌘O
Go to file	查找文件	Ctrl + Shift + N	⌘⇧O
Go to symbol	查找变量	Ctrl + Alt + Shift + N	⌘⌥O
Go to next /previous editor tab	上一个/下一个选项卡	Alt + Right/Left	⌃LEFT/⌃RIGHT
Go back to previous tool window	回到以前的工具窗口	F12	F12
Go to editor (from tool window)	回到编辑窗口	Esc	ESC
Hide active or last active window	隐藏活动窗口或最后一个活动窗口	Shift + Esc	⇧ESC
Go to line	跳转到行	Ctrl + G	⌘L
Recent files popup	最近文件弹出	Ctrl + E	⌘E
Navigate back/ forward	后退/前进	Ctrl + Alt + Left/Right	⌘⌥LEFT/⌘⌥RIGHT
Navigate to last edit location	导航到最后编辑的位置	Ctrl + Shift + Backspace	⌘⇧BACKSPACE
Select current file or symbol in any view	选择当前文件或变量到任何视图	Alt + F1	⌥F1
Go to declaration	跳转到声明处	Ctrl + B ,Ctrl + Click	⌘B,⌘CLICK
Go to implementation(s)	跳转到实现	Ctrl + Alt + B	⌘⌥B
Open quick definition lookup	打开快速定义查找	Ctrl + Shift + I	⌥SPACE,⌘Y
Go to type declaration	去类型声明	Ctrl + Shift + B	⌃⇧B
Go to super-method/ super-class	父类方法/超类	Ctrl + U	⌘U
Go to previous /next method	去之前/下一个方法	Alt + Up/Down	⌃UP/⌃DOWN
Move to code block end/ start	移动到代码块开始/结束	Ctrl + ]/[	⌘]/⌘[
File structure popup	弹出文件结构	Ctrl + F12	⌘F12
Type hierarchy	类型层次结构	Ctrl + H	⌃H
Method hierarchy	方法的层次结构	Ctrl + Shift + H	⌘⇧H
Call hierarchy	调用层次结构	Ctrl + Alt + H	⌃⌥H
Next /previous highlighted error	下一个/以前的错误高亮显示	F2 / Shift + F2	F2/⇧F2
Edit source/View source	编辑源/查看源代码	F4 / Ctrl + Enter	F4/⌘DOWN
Show navigation bar	显示导航栏	Alt + Home	⌥Home
Toggle bookmark	增加/取消书签	F11	F3
Toggle bookmark with mnemonic	使用助记符增加/取消书签	Ctrl + F11	⌥F3
Go to numbered bookmark	跳转到数字书签	Ctrl + #[0-9]	⌃0…⌃9
Show bookmarks	显示书签	Shift + F11	⌘F3
Compile and Run
Make project	Make project	Ctrl + F9	⌘F9
Compile selected file, package or module	选择编译文件,包或模块	Ctrl + Shift + F9	⌘⇧F9
Select configuration and run/debug	选择配置和运行/调试	Alt + Shift + F10/F9	⌃⌥R/D
Run/Debug	运行/调试	Shift + F10 / F9	⌃R/D
Run context configuration from editor	运行环境配置从当前编辑器	Ctrl + Shift + F10	⌃⇧R,⌃⇧D
Usage Search
Find usages /Find usages in file	发现使用/用法在文件	Alt + F7 / Ctrl + F7	⌥F7/⌘F7
Highlight usages in file	强调使用的文件	Ctrl + Shift + F7	⌘⇧F7
Show usages	显示使用	Ctrl + Alt + F7	⌘⌥F7
VCS / Local History
Commit project to VCS	提交项目到VCS	Ctrl + K	⌘K
Update project from VCS	更新项目从VCS	Ctrl + T	⌘T
Push commits	推送提交	Ctrl + Shift + K	⌘⇧K
‘VCS’ quick popup	VCS快速操作	Alt + BackQuote (`)	⌃V
Live Templates
Surround with Live Template	使用模板环绕	Ctrl + Alt + J	⌘⌥J
Insert Live Template	插入模板	Ctrl + J	⌘J


## window

|文件操作|快捷键|操作|
|-|-|-|
|文件查找|全局查找任意文件|Shift+Shift|
|文件查找|查找最近文件|Ctrl+E/Ctrl+Shift+E|
|文件查找|查找类文件|Ctrl+N|
|文件结构|生成相关的结构图|Ctrl+Alt+U|
|文件结构|文件方法结构|Ctrl+F12|
|文件切换|按Tab文件向左或右切换|Alt+left/right|

|光标操作|快捷键|操作|
|-|-|-|
|光标移动|移动之前或之后位置|Ctrl+Alt+left/right|
|光标移动|上下快速移动|Alt+Up / Alt+Down|
|光标移动|左右快速移动|Ctrl+left / Ctrl+Right|

|代码操作|快捷键|操作|
|-|-|-|
|代码生成|补全代码|Alt+Insert|
|代码生成|包裹代码|Ctrl+Alt+T|
|代码生成|生成测试代码|Ctrl+Shift+T|
|代码整理|格式化代码|Ctrl+Alt+L|
|代码编辑|复制当前行，光标下移|Ctrl+D|
|代码编辑|删除当前行，光标下移|Ctrl+Y|
|代码编辑|向下插入新行|Shift+Enter|
|代码编辑|向上插入新行|Ctrl+Alt+Enter|
|代码编辑|大小写转换|Ctrl+Shift+U|
|代码编辑|选择相同变量名，统一修改|Alt+j|


|注释操作|快捷键|操作|
|-|-|-|
|注释添加|添加或取消//注释|Ctrl+/|
|注释添加|添加或取消/**/注释|Ctrl+Shift+/|


|窗口操作|快捷键|操作|
|-|-|-|
|切换窗口|项目结构窗口|Alt+1|
|切换窗口|文件结构窗口|Alt+7|

|建构操作|快捷键|操作|
|-|-|-|
|编译|运行|Shift+F10|
|编译|选择运行操作|Alt + Shift + F10|
|编译|Debug|Shift+F9|
|编译|选择debug操作|Alt + Shift + F9|

## mac
返回上一步光标位置： command+option+left
返回下一步光标位置：command+option+right


整行删除， command+删除键
整行拷贝， command+D

control+D. debug 运行
control+R。 运行

25.格式化代码
Command + Option + L

代码包围快捷键, 可在选种代码种插入， if, for, while, try catch finally
win:ctrl+alt+T
mac:Command+option+T

按选种的代码，重复选择
win:window+alt+J
mac:control+G

10.快捷弹出一个类的结构:
win:alt+7
mac:Command + 7
mac:Command + 1 project 窗口


11.搜索文件, class：
Shift 快速连续按两下

6.错误解决方案快捷键:
win:alt+enter
mac:option+enter

4.快捷添加代码，gettter setter， contrunction 等
win:alt + insert
mac:Command + N

15.弹出当前打开的文件列表
Command + E

