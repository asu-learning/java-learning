/**
 * @author : zhenyun.su
 * @since : 2023/9/15
 */

import java.util.Set;
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

@SupportedAnnotationTypes("com.example.MyAnnotation")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MyAnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : annotatedElements) {
                MyAnnotation myAnnotation = element.getAnnotation(MyAnnotation.class);
                if (myAnnotation != null) {
                    // 在这里对注解进行处理
                    System.out.println("Found MyAnnotation with value: " + myAnnotation.value());
                }
            }
        }
        return true; // 表示注解处理器已处理所有注解
    }
}
