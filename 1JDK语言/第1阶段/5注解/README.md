# 7注解

		What is Annotation
		how to use
		Annotation Purposes
		Creating Your Own Annotations
		有什么用呢？如何实现我想要的逻辑

## 什么是注解（Annotation）

在Java中，注解（Annotation）是为代码添加元数据（metadata）的机制。代码可以是类，接口，及注解

它们提供有关代码的信息，例如类、方法或变量的用途、作者等。
这些信息可以被编译器、开发工具或其他代码使用，以实现各种功能。

比如经常定义类实体使用的注解，如下
```java
@Entity
@Table(name = "tt_sale_header", schema = "dsale")
public class SaleHeader implements Serializable {
}
```
@Entity表明为一个实体类， 
@Table表明为数据库表的映射数据，比如表名，在哪个schema下，设置索引，约束的定义

定义如下
```java
package javax.persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})   // 用于在class上
@Retention(RetentionPolicy.RUNTIME) //在运行时，提供说明
public @interface Table {
    String name() default "";

    String catalog() default "";

    String schema() default "";

    UniqueConstraint[] uniqueConstraints() default {};

    Index[] indexes() default {};
}
```

## 注解的用途

Java注解通常用于以下目的：
1. 编译器指令
2. 构建时指令
3. 运行时指令（重点学习）

### 1. 编译器指令

#### 什么是编译指令
编译器指令是编译器在编译源代码时执行的指令。它们用于将源代码转换为目标代码，以便计算机可以执行。

编译器指令通常包括以下几种类型：
1. 预处理指令：在编译器开始编译之前执行，例如包含头文件、定义宏等。常见的预处理指令有#include、#define等。
2. 词法分析指令：负责将源代码分解成有意义的符号（tokens）。它们识别出变量名、关键字、运算符等，并将其分类为不同的语法元素。
3. 语法分析指令：根据语言的语法规则来检查词法分析生成的符号序列是否符合语法要求。如果发现语法错误，编译器会报告错误并停止编译过程。
4. 语义分析指令：检查源代码中的语义错误，例如类型不匹配、未声明的变量等。如果发现语义错误，编译器也会报告错误并停止编译过程。
5. 优化指令：对生成的目标代码进行优化，以提高程序的执行效率。优化可以包括常量折叠、死代码消除、循环优化等。
6. 目标代码生成指令：将经过上述步骤生成的中间表示转换为目标机器代码或汇编代码。目标代码可以是机器码、字节码或汇编代码，具体取决于目标平台和编程语言的特性。

总之，编译器指令在编译过程中起到了将源代码转换为可执行代码的作用，确保源代码的正确性和高效性。

#### Java编译指令

Java的编译器指令是javac，它是Java编译器的命令行工具。使用javac可以对Java源代码文件进行编译，将其转换为字节码文件（.class文件）。

以下是使用javac编译Java源代码的基本语法： 
```bash
javac FileName.java
```
其中，FileName.java是要编译的Java源代码文件名。如果源代码文件位于当前目录下，可以只提供文件名。

javac命令会自动查找源代码文件中的包声明（package declaration）和导入的类文件，并生成相应的字节码文件。如果源代码中引用了其他包中的类，需要使用-cp或-classpath选项指定类路径（classpath），以便编译器能够找到所需的类文件。

以下是一些常用的javac选项：
```
    -d <directory>：将生成的字节码文件输出到指定的目录。
    -sourcepath <path>：指定源代码文件的搜索路径，可以是多个路径，用分号分隔。
    -encoding <encoding>：指定源代码文件的字符编码，例如UTF-8。
    -verbose：显示详细的编译过程信息。
    -help：显示javac的帮助信息。
```
除了基本选项，javac还支持许多其他的选项和特性，可以根据具体的需求查阅相关文档进行了解和使用。

#### Java注解如何影响编译指令
Java注解在编译过程中主要通过编译器指令来起作用。编译器在编译源代码时会读取注解，并根据注解的信息对代码进行处理和生成相应的字节码文件。

具体来说，Java注解可以影响编译指令的方式如下：

1. 包声明（Package declaration）：如果源代码中包含了包声明，编译器会在生成的字节码文件中保留该包声明信息。这意味着在使用反射或其他需要解析包结构的操作时，字节码文件中的包声明信息将起到重要的作用。
2. 导入类（Import statements）：如果源代码中使用了import语句导入其他包中的类，编译器会将这些类文件包含在编译过程中。这样，在生成的字节码文件中就包含了这些类的符号信息，以便在运行时可以通过反射机制找到并使用这些类。
3. 类、接口和方法注解（Class, interface, and method annotations）：如果源代码中的类、接口或方法上使用了注解，编译器会读取这些注解信息，并在生成字节码文件时进行相应的处理。例如，注解中可能指定了类、接口或方法的一些属性或行为，编译器会根据这些信息生成相应的字节码指令。
4. 编译时织入（Compile-time weaving）：有些Java注解处理器可以在编译时对源代码进行处理，修改源代码或生成额外的代码。这种编译时织入的过程会改变源代码的内容，并影响编译器的编译指令。

需要注意的是，Java注解本身并不直接影响编译器的编译指令，而是通过编译器读取注解信息并对源代码进行处理来实现相应的功能。注解本身只是提供了一种元数据的形式，用于向编译器传递额外的信息。

Java注解可以放置在类、接口、方法、方法参数、字段和局部变量的上方。

#### 通过一个实例，在编译时读取类的注解并进行处理
当编译时读取类的注解并进行处理，可以使用Java的注解处理器（Annotation Processor）。
注解处理器是一种在编译阶段运行的工具，可以读取源代码中的注解信息，并对这些注解进行处理。

定义编译时的注解
```java
import java.lang.annotation.*;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface MyAnnotation {
    String value() default "";
}

```

注解处理器
```java
import java.util.Set;
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;

@SupportedAnnotationTypes("com.example.MyAnnotation")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MyAnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : annotatedElements) {
                MyAnnotation myAnnotation = element.getAnnotation(MyAnnotation.class);
                if (myAnnotation != null) {
                    // 在这里对注解进行处理
                    System.out.println("Found MyAnnotation with value: " + myAnnotation.value());
                }
            }
        }
        return true; // 表示注解处理器已处理所有注解
    }
}

```
MyAnnotationProcessor类使用了@SupportedAnnotationTypes和@SupportedSourceVersion注解来指定它支持处理的注解类型和Java源代码版本。
然后，通过重写process方法，在该方法中遍历所有的注解元素，找到被MyAnnotation注解的元素，并读取注解的值进行处理

```java
@MyAnnotation
public class MyClass {
    public static void main(String[] args) {
        MyClass m = new MyClass();
        System.out.println("test");
    }
}
```

```sh
  javac -processorpath /path/to/annotation/processor com.example.MyClass.java
```

### 2. 构建时指令

#### Java构建指令
Java的构建指令主要包括以下几种：

1. 编译指令：javac，用于将Java源代码文件编译成字节码文件（.class文件）。
2. 打包指令：jar，用于将多个类文件和资源文件打包成一个可执行的JAR文件。
3. 签名指令：jarsigner，用于对JAR文件进行数字签名，以确保代码的完整性和安全性。
4. 混淆指令：proguard，用于对Java代码进行混淆和优化，以提高代码的安全性和反编译难度。
5. 调试指令：javap，用于查看Java虚拟机的字节码指令和常量池信息。
6. 测试指令：junit，用于编写和运行单元测试和集成测试。
7. 文档生成指令：javadoc，用于从Java源代码文件中提取注释和代码结构信息，生成API文档。
8. 部署指令：java，用于在服务器上运行已打包的应用程序。

#### Java注解如何影响构建指令

Java注解可以影响构建指令的方式有以下几种：

1. 通过注解处理器生成额外的构建指令。例如，使用@BuildPlugin注解可以让Maven插件在构建过程中执行自定义的构建逻辑，并生成相应的构建指令。
2. 通过注解修改构建过程的行为。例如，使用@Test注解可以告诉构建工具对指定的测试类进行特定的构建操作，如运行单元测试或集成测试。
3. 通过注解提供构建信息。例如，使用@Description注解可以为构建信息文件提供描述信息，以便在构建过程中显示更详细的日志和警告信息。


使用@SpringBootApplication注解可以将一个普通的Java应用程序转换为一个可执行的Spring Boot应用程序，并自动配置应用程序的构建目标和依赖项。

#### 提供一个实例，Java注解如何影响构建指令

通过AI来回答

总之，Java注解可以在构建过程中发挥重要的作用，帮助开发人员更好地控制和管理构建过程。

#### 内置构建时的注解
* @Deprecated
* @Override
* @SuppressWarnings

##### @Deprecated
The @Deprecated annotation is used to mark a class, method or field as deprecated, meaning it should no longer be used

```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value={CONSTRUCTOR, FIELD, LOCAL_VARIABLE, METHOD, PACKAGE, PARAMETER, TYPE})
public @interface Deprecated {
}

@Deprecated
public class MyComponent {
}
```

If your code uses deprecated classes, methods or fields, the compiler will give you a warning
warning MyComponent 已过期

##### @Override
The @Override Java annotation is used above methods that override methods in a superclass.

If the method does not match a method in the superclass, the compiler will give you an error.

##### @SuppressWarnings

The @SuppressWarnings annotation makes the compiler suppress warnings for a given method. For instance,

if a method calls a deprecated method, or makes an insecure type cast, the compiler may generate a warning.

You can suppress these warnings by annotating the method containing the code with the @SuppressWarnings annotation.


### 3. 运行时指令

#### Java运行时指令
Java运行时指令是指在Java虚拟机（JVM）中执行的指令。
这些指令可以用于执行Java程序中的代码，并控制程序的执行流程。以下是一些常见的Java运行时指令：

1. 加载类文件：使用ClassLoader类的loadClass()方法将字节码文件加载到JVM中。
2. 实例化对象：使用Constructor类的newInstance()方法创建类的实例。
3. 访问字段和方法：使用Field类和Method类的get()和invoke()方法访问对象的字段和方法。
4. 抛出异常：使用Throwable类的throw()方法抛出异常。
5. 垃圾回收：使用Runtime类的gc()方法触发垃圾回收。
6. 同步和锁：使用synchronized关键字实现同步和互斥。
7. 反射：使用Class类的forName()方法获取类的元数据信息，并使用Constructor类、Method类等进行反射调用。
8. 动态代理：使用Proxy类的newProxyInstance()方法创建动态代理对象。
9. IO操作：使用FileInputStream、FileOutputStream、ObjectInputStream、ObjectOutputStream等类进行文件读写操作。

总之，Java运行时指令是JVM中执行的指令，用于控制Java程序的执行流程，实现Java程序的各种功能。

#### Java注解如何影响运行时指令

Java注解可以影响运行时指令的方式有以下几种：

1. 通过注解处理器生成额外的运行时指令。例如，使用@PostConstruct注解可以让Spring框架在Bean初始化完成后自动执行特定的方法，并生成相应的运行时指令。
2. 通过注解控制运行时行为。例如，使用@PreDestroy注解可以在Bean销毁之前自动执行特定的方法，如关闭资源或清理缓存。
3. 通过注解修改运行时行为。例如，使用@Around注解可以实现AOP（面向切面编程），在目标方法执行前后添加特定的逻辑，并生成相应的运行时指令。
4. 通过注解提供运行时信息。例如，使用@LogExecutionTime注解可以记录方法的执行时间，并在日志中输出相关信息，以便进行性能分析和调优。

总之，Java注解可以在运行时发挥重要的作用，帮助开发人员更好地控制和管理程序的行为和性能。

#### 提供一个实例，Java注解如何影响运行时指令
        

## 自定义注解

讲运行时注解来讲

```java
public @interface MyAnnotation {
    String   value() default "";
    String   name();
    int      age();
    String[] newNames();
}
```

Notice the @interface keyword. This signals to the Java compiler that this is a Java annotation definition

### To use the above annotation
```java
@MyAnnotation(
    name="Jakob",
    age=37,
    newNames={"Jenkov", "Peterson"}
)
public class MyClass {
}

```

### custom Annotation can use
1. @Retention - jvm是否加载并读取
2. @Target    - 应用类，还是方法，还是成员上
3. @Inherited - 注解可被继承
4. @Documented - 将注解添加到javadoc


#### @Retention
You can specify for your custom annotation if it should be available at runtime, for inspection via reflection. You do so by annotating your annotation definition with the @Retention annotation. Here is how that is done:

```java
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation {
    String   value() default "";
}
```

RetentionPolicy.RUNTIME

Annotations are to be recorded in the class file by the compiler and retained by the VM at run time, so they may be read reflectively.

* RetentionPolicy.SOURCE  //注解在编译时就被丢弃， 这个选项不知道有什么用？
* RetentionPolicy.CLASS   //注解在编译时记录在class文件中，在运行时不被加载到JVM中
* RetentionPolicy.RUNTIME //注解在编译时记录在class文件中，在运行时被加载到JVM中，利用反射机制读取


#### @Target
You can specify which Java elements your custom annotation can be used to annotate

```java
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
public @interface MyAnnotation {
    String   value();
}
```

The ElementType class contains the following possible targets:

* ElementType.ANNOTATION_TYPE //注解上
* ElementType.CONSTRUCTOR //构造方法上
* ElementType.FIELD //成员上
* ElementType.LOCAL_VARIABLE  //局部成员上
* ElementType.METHOD //method
* ElementType.PACKAGE  //package
* ElementType.PARAMETER //method 参数上
* ElementType.TYPE   //class

#### @Inherited
自定义注释可以跟随类被继承
java.lang.annotation.Inherited
```
@Inherited
public @interface MyAnnotation {
}

@MyAnnotation
public class MySuperClass { ... }
public class MySubClass extends MySuperClass { ... }
```

#### @Documented
自定义注释也可也在Java Doc中看到
```java
import java.lang.annotation.Documented;

@Documented
public @interface MyAnnotation {

}
@MyAnnotation
public class MySuperClass { ... }
```

When generating JavaDoc for the MySuperClass class,

the @MyAnnotation is now included in the JavaDoc.

You will not use the @Documented annotation often,

but now you know it exists, if you should need it.

```java
package xyz.erupt.cloud.server.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.EruptI18n;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.CodeEditorType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.jpa.model.MetaModel;

import javax.persistence.*;

/**
 * @author YuePeng
 * date 2021/12/16 00:32
 */
@Getter
@Setter
@Entity
@Table(name = "e_cloud_node_group", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
@Erupt(name = "分组配置")
@EruptI18n
public class CloudNodeGroup extends MetaModel {

    @EruptField(
            views = @View(title = "编码", sortable = true),
            edit = @Edit(title = "编码", notNull = true, search = @Search(vague = true))
    )
    private String code;

    @EruptField(
            views = @View(title = "名称", sortable = true),
            edit = @Edit(title = "名称", notNull = true, search = @Search(vague = true))
    )
    private String name;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @EruptField(
            views = @View(title = "分组配置"),
            edit = @Edit(title = "分组配置", type = EditType.CODE_EDITOR, codeEditType = @CodeEditorType(language = "json"))
    )
    private String config;

    @Column(length = 5000)
    @EruptField(
            views = @View(title = "描述", type = ViewType.HTML),
            edit = @Edit(title = "描述", type = EditType.TEXTAREA)
    )
    private String remark;

}

```
