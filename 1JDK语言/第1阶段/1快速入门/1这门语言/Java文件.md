# Java文件

## java结构讲解


java文件实例代码如下

```java
// 1. java文件所在包
package com.af.system.configurer;

// 2. 导入其他java文件，可以类，接口，枚举，静态常量， * 代表目录下所有文件

import static me.chanjar.weixin.channel.constant.WxChannelApiUrlConstants.GET_STABLE_ACCESS_TOKEN_URL;

import com.af.properties.WxChannelProperties;
import com.ribo.x3ext.properties.WxcpProperties;
import me.chanjar.weixin.channel.config.WxChannelConfig;
import me.chanjar.weixin.channel.config.impl.WxChannelDefaultConfigImpl;
import me.chanjar.weixin.cp.api.impl.WxCpServiceOkHttpImpl;
import me.chanjar.weixin.cp.config.WxCpConfigStorage;
import me.chanjar.weixin.cp.config.impl.WxCpDefaultConfigImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author : zhenyun.su
 * @comment : 企业微信配置类
 *
 * @since : 2020/03/02
 */

// 本类型定义
@Configuration
public class AfWxChannelConfigurer {
    private WxChannelProperties properties;

    public AfWxChannelConfigurer(WxChannelProperties properties) {
        this.properties = properties;
    }

    @Bean
    public WxChannelDefaultConfigImpl wxCpDefaultConfig(){
        WxChannelDefaultConfigImpl config = new WxChannelDefaultConfigImpl();
        config.setAppid(properties.getAppId());
        config.setSecret(properties.getAppSercet());
        config.setToken(properties.getToken());
        config.setAesKey(properties.getAesKey());
        return config;
    }

    @Bean
    public WxCpServiceOkHttpImpl wxCpServiceOkHttp(){
        WxChannelConfig wxCpServiceOkHttp = new WxCpServiceOkHttpImpl();
        wxCpServiceOkHttp.setWxCpConfigStorage(wxCpDefaultConfig());
        return wxCpServiceOkHttp;
    }
}
```

