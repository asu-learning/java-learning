# 注释与文档

    注释表示
    注释文档
      注释语法
      常用标签

## 简述
java的注释除了更好理解代码外，可利用javadoc工具生成java技术html文档。方便随时查看。

Java 支持 3 种注释
1. //   单行注释一行
2. /*    */ 多行注释 
3. /**  ……*/ 文档注释，支持写入Javadoc文档。

关键说明：多行注释中出现其他* 也被注释

详细说明文档注释
## 文档注释

可基于类、接口、成员变量及方法来注释，因为 Javadoc 只处理这些地方的文档注释，而忽略其它地方的文档注释

先看实例
```java
/**
* @author C语言中文网
* @version jdk1.8.0
*/
public class Test{
    /**
    * 求输入两个参数范围以内整数的和
    * @param n 接收的第一个参数，范围起点
    * @param m 接收的第二个参数，范围终点
    * @return 两个参数范围以内整数的和
    */
    public int add(int n, int m) {
        int sum = 0;
        for (int i = n; i <= m; i++) {
            sum = sum + i;
        }
        return sum;
    }
}
```

关键说明：
1. 注释命令，只能出现"/**" 之后，注释结束于"*/"
2. `文档注释支持HTML标签，比如：需要换行时，应该使用<br>，而不是一个回车符；需要分段时，应该使用<p>`
3. @开头东东被称之为Javadoc文档标记，是JDK定义好的，如@author、@version、@since、@see、@link、@code、@param、@return、@exception、@throws等。    
4. javadoc工具只能提取**public和protected**的成员和方法，因为这些才是提供客户端使用

### 常用标签

```
@author： 显示作者信息，格式 @author author_information
@version： 格式 @version version_information 显示版本信息


@param: 用于方法上，指定方法参数，格式 @param param_name description
@return: 用于方法上，指定返回值，格式 @return  description

@see： 引用其他类，允许用户引用其他类的文档。生成的是html的超链接标签
@docRoot： 将注释生成到根文档，用于文档树页面的显示超链接
@Deprecated : 表示该类或方法，已有新类和方法替代，将会在以后的版本中不在使用
```

### Javadoc

使用javadoc工具，从java文件中提取注释，并生成html文档

javadoc工具是JDK安装的一部分

Javadoc 用法格式如下： 
javadoc [options] [packagenames] [sourcefiles]

对格式的说明：
1. options 表示 Javadoc 命令的选项；
2. packagenames 表示包名；
3. sourcefiles 表示源文件名

实例说明
```shell
# 到Test所在目录，执行下命令
javadoc -author -version Test.java
# 存在中文，需要指定编码格式
javadoc -encoding UTF-8 -charset UTF-8  Test.java

# 查看更多Javadoc 的用法和选项
javadoc -help
```

更多选项
```
-public	        仅显示 public 类和成员
-protected	显示 protected/public 类和成员（默认值）
-package	显示 package/protected/public 类和成员
-private	显示所有类和成员
-d <directory>	输出文件的目标目录
-version	包含 @version 段
-author	        包含 @author 段
-splitindex	将索引分为每个字母对应一个文件
-windowtitle <text>	文档的浏览器窗口标题
```

很多IDE工具集成了javadoc，可方便使用

通过IDEA生成Javadoc ： Tools -> Generate JavaDoc


## 阿里巴巴Java开发手册之注释规约

类，类属性、类方法的注释必须使用Javadoc规范，使用/*内容/格式，不得使用//XX方式。

所有的抽象方法（包括接口中的方法）必须要用Javadoc注释，除了返回值、参数、异常说明外，还必须指出该方法做什么事情，实现什么功能。

所有的类都必须添加创建者和创建日期。

方法内部的单行注释，在被注释语句上方另起一行，使用//注释。方法内部的多行注释，使用/* */注释，注意与代码对齐。

所有的枚举类型字段必须要有注释，说明每个数据项的用途。

与其用不熟练的英文来注释，不如用中文注释把问题说清楚。专有名词与关键字保持英文原文即可。

在修改代码的同时，要对注释进行相应的修改，尤其是参数，返回值，异常，核心逻辑等的修改。

谨慎注释掉代码。要在上方详细说明，而不是简单的注释掉。如果无用，则删除。

对于注释的要求：

（1）能够准确反映设计思想和代码逻辑。

（2）能够描述业务含义，使其他程序员能够迅速了解代码背后的信息。完全没有注释的大段代码对于阅读者形同天书；注释是给自己看的，应做到即使间隔很长时间，也能清晰理解当时的思路，注释也是给继任者看的，使其能够快速接替自己的工作。

好的命名、代码结构是自解释的，注释力求精简准确、表达到位。避免出现注释的一个极端：过多过滥的注释，因为代码的逻辑一旦修改，修改注释是相当大的负担。

特殊注释标记，请标明标记人与标记时间。注意及时处理这些标记，通过标记扫描经常处理此类标记。有时候线上故障就来源于这些标记处的代码。

作者：花伤情犹在
链接：https://www.jianshu.com/p/3c74c62fafae
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。