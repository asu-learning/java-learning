# cola分层设计

## 为什么需要分层设计
大家都知道混乱代码结构不利于维护，不利用扩展和复用。特别业务迭代快，工期紧，更会出现混乱代码结构，另外维护时间周期越长，维护人员变更越多，代码结构更加混乱，更加不易维护，长此以往恶性循环最终导致系统被迫下线。

如何避免呢？
1. 遵循一个标准，以搭建一个具有整洁，清晰代码结构的系统。
2. 在维护系统时，保持遵循这个标准。
3. 定期遵循这个标准来重构代码。

为了方便多人进行技术设计，开发及测试，需要一个标准化的代码架构

这个标准就是在领域驱动设计(DDD)思想下的具有最佳实践的代码分层分包架构模型COLA， 可解决代码架构降低耦合、高复用、分而治之。

## 什么是COLA

COLA为Clean Object-Oriented and Layered Architecture的英文缩写
中文意思`整洁面向对象分层架构`. 是来自阿里技术专家的[开源项目](https://github.com/alibaba/COLA)

该项目即提供代码分层分包架构思想，还提供代码的最佳实践及可服用组件。

下面重点说明COLA架构思想及如何使用
### COLA架构思想

#### 传统分层架构
![Img](./2828a142.png)
特点： 把系统分为表示层、业务逻辑层、数据访问层
考虑如下情况，就可以了解存在缺点：
1. 否有过困惑，代码到底应该放在哪个层，虽然定义了各层的职责，但是总有人不严格遵循层次的分界，比如业务逻辑写在了表示层，或者业务逻辑直接和数据访问绑定。
2. 对接消息中间件，调度访问及redis，这种一维的上下依赖层次就无法适应了。
深入思考后，传统分层架构容易时的系统代码变的臃肿，可扩展性和可伸缩性差。

#### 更好的架构模型COLA
先来看两张官方介绍图

总体分层依赖流程图
![Img](./f3c61a69.png)
明细分层分包一览图
![Img](./a3c9f354.png)
分层说明：
1. adapter层：适配器层相当传统controller层，为不同外部提供不同适配器
2. app层：应用层，基于服务接口实现业务逻辑的应用层
3. client层：client层也称为SDK层，定义服务接口和dto，承接adapter层到app层
4. domain层：领域层定义领域对象，提供领域服务。比如零售订单领域
5. infrastructure层：基础设施层表示数据库，redis等，比如零售订单拆分主从表分别操作数据库。

COLA模型特点： 代码依赖关系更加松散，代码模块化，更具有复用性，可分而治之。

在上图中还可以看出COLA分为两个部分，
1. COLA架构最佳实践方法
2. COLA通用组件，可直接依赖使用


分层分包一览图：
| 层次       | 包名        | 功能                            | 必选 |
| ---------- | ----------- | ------------------------------- | ---- |
| Adapter层  | web         | 处理页面请求的Controller        | 否   |
| Adapter层  | wireless    | 处理无线端的适配                | 否   |
| Adapter层  | wap         | 处理wap端的适配                 | 否   |
| App层      | executor    | 处理request，包括command和query | 是   |
| App层      | consumer    | 处理外部message                 | 否   |
| App层      | scheduler   | 处理定时任务                    | 否   |
| Domain层   | model       | 领域模型                        | 否   |
| Domain层   | ability     | 领域能力，包括DomainService     | 否   |
| Domain层   | gateway     | 领域网关，解耦利器              | 是   |
| Infra层    | gatewayimpl | 网关实现                        | 是   |
| Infra层    | mapper      | ibatis数据库映射                | 否   |
| Infra层    | config      | 配置信息                        | 否   |
| Client SDK | api         | 服务对外透出的API               | 是   |
| Client SDK | dto         | 服务对外的DTO                   | 是   |

## 分层分包详细说明
通过实例af-cola-demo项目来说明分层包架构，通过多模块，maven父子项目来构建

代码模块如下
![Img](./ea1385ad.png)

### start层
只负责整个SpringBoot应用的启动模块，添加所有子模块的依赖，添加全局的配置。
![Img](./62ed7134.png)
将启动独立出来，好处是清晰简洁，其他模块都有以插件的形式按需加载。


### adapter层
面向外部的适配器层，相当传统controller层，针对不同外部端实现不同适配器，不涉任何业务逻辑，按外部端类型分包，有pc web, mobile, wap(慢慢被淘汰)。
![Img](./4d4ba129.png)

### app层
基于服务接口实现业务逻辑的应用层，需获取输入，组装上下文，参数校验，然后调用领域层或基础设施层完成数据更新，是先按照业务分包，再按照功能分包的，功能分包(executor-处理request请求, consumer-处理订阅消息, scheduler-处理调度请求)
![Img](./ed6d8d1f.png)

### client层
定义服务接口和dto，承接adapter层到app层，client层也可称为SDK层，可将打包JAR供其他模块或系统使用。
![Img](./b08c3b0c.png)

api文件夹：存放服务接口定义
dto文件夹：存放传输实体

### domain层 
领域层封装了核心业务逻辑，实现领域服务（Domain Service）和领域对象（Domain Entity），为App层提供领域实体和领域服务，先按照业务分包，再按照功能分包的(domainservice-领域服务层，gateway-基础实施层网关服务层， 领域实体)
![Img](./301aa884.png)

1. 领域实体：实体模型可以是充血模型，还不太明白？？
2. 领域服务：domainservice文件夹下，是领域对外暴露的服务能力，如上图中的CreditChecker
3. 领域网关：gateway文件夹下的接口定义，这里的接口你可以粗略的理解成一种SPI，也就是交给infrastructure层去实现的接口。


关键说明：domainservice用于提供领域服务，比如零售订单领域，包含零售主从表，会员信息。然后在gateway目录分别来实现，零售主表的网关等

CustomerGateway里定义了接口findById，要求infrastructure的实现类必须定义如何通过消费者Id获取消费者实体信息，而infrastructure层可以实现任何数据源逻辑，比如，从MySQL获取，从Redis获取，还是从外部API获取等等


### infrastructure层
基础设施层负责第三方底层技术处理，比如数据库的CRUD、搜索引擎、文件系统、分布式服务的RPC等。domain层的定义gateway也在这里实现，才能被上面的App层或domain层调用， 按gatewayimpl。config, mapper等方式分层，不同基础设施，不同实现方式，分层会有所不同。
![Img](./1aaaed8b.png)


## 归纳总结Cola

### 1. 领域与功能的分包策略
![Img](./c623920a.png)

上图先按照领域分包，再按照功能分包，这样做的其中一点好处是能将腐烂控制在该业务域内，比如消费者customer和订单order两个领域是两个后端开发并行开发，两个人对于dto，util这些文件夹的命名习惯都不同，那么只会腐烂在各自的业务包下面，而不会将dto,util,config等文件夹放在一起，极容易引发文件冲突。

### 2. 业务域和外部依赖解耦
前面提到的domain和infrastructure层的依赖倒置，是一个非常有用的设计，进一步解耦了取数逻辑的实现

例如下图中，你的领域实体是商品item，通过gateway接口，你的商品的数据源可以是数据库，也可以是外部的服务API。

如果是外部的商品服务，你经过API调用后，商品域吐出的是一个大而全的DTO（可能包含几十个字段），而在下单这个阶段，订单所需要的可能只是其中几个字段而已。你拿到了外部领域DTO，转为自己领域的Item，只留下标题价格库存等必要的数据字段。
![Img](./afed034b.png)

### 没有最完美只有最合适代码架构

阿里COlA给我们提供一个整洁面向对象分层架构思想和实践，但它不可能完美应用所有的项目，一个好的分层分包的代码架构，需要基于业务，技术生态来设计的。

## 参考资料

[如何保证同事的代码不会腐烂？一文带你了解 Alibaba COLA 架构](
https://www.cnblogs.com/rude3knife/p/cola-architecture.html?share_token=a7c45034-e7bf-4574-8b1b-31c4377167f3)

## 下一步行动