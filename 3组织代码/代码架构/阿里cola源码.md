# 阿里cola源码


## 阿里cola简介

官网地址： https://github.com/alibaba/COLA
项目为我们提供cola分层架构的实例代码，cola的maven模版及cola组件。
1. cola分层架构的实例代码，方便大家直接查看cola架构
2. 可基于cola的maven模版，快速建构标准cola架构项目
3. cola组件为项目提供方便公用代码。

cola的maven模版包含两种项目
1. cola-archetype-service：用来创建纯后端服务的archetype。
2. cola-archetype-web：用来创建adapter和后端服务一体的web应用archetype。

## 构建cola项目
可通过下面三种方法来构建
1. 不推荐使用：https://start.aliyun.com/bootstrap.html 生成cola应用， 原因不是最新的cola应用代码
2. 推荐使用获取cola模版项目，理由方便，按官方执行即可。
3. 推荐使用源码编译打包，并安装到本地maven，在使用命令或idea来构建cola架构的项目。

### 直接使用中央仓库cola模版项目

命令参考：https://github.com/alibaba/COLA

### 构建archetype模版，并导入到idea
1. 首先获取cola源码：https://github.com/alibaba/COLA
2. 构建maven的archetype模版项目
   运行命令：mvn archetype:create-from-project， target目录下生成cola-framework-archetype-service-archetype-4.0.1.jar

3. 将模版项目安装到maven
   进入到target/generated-sources/archetype目录下
   运行mvn install，将这个jar安装到本地仓库，即可用archetype来生成项目。
   执行mvn deploy，安装到中央仓库，就可以共享给别来生成项目。
4. 添加自定义的archetype模版
   在IDEA中添加上述编译生成的COLA项目模板，file -> new project->maven -> create from archetype
   选择cola-framework-archetype-service-archetype-4.0.1.jar
   填写groupid、artifactid、version 信息
   `<groupId>com.alibaba.cola</groupId>
   <artifactId>cola-framework-archetype-service</artifactId>
   <version>4.0.1</version>`

使用idea构建构建cola项目， file->new -> 选择maven，选择cola-framework-archetype-service或cola-framework-archetype-web
填写本项目名称即可。

## 实例说明

构建cola分层架构的af-demo项目


## 参考资料

[阿里开源COLA 4.0源码编译和部署过程](https://www.cnblogs.com/rickie/p/15415016.html)

[如何保证同事的代码不会腐烂？一文带你了解 Alibaba COLA 架构](
https://www.cnblogs.com/rude3knife/p/cola-architecture.html?share_token=a7c45034-e7bf-4574-8b1b-31c4377167f3)