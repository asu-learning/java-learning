# cola分层设计

## 为什么需要分层设计
大家都知道混乱代码结构不利于维护，不利用扩展和复用。特别业务迭代快，工期紧，更会出现混乱代码结构，另外维护时间周期越长，维护人员变更越多，代码结构更加混乱，更加不易维护，长此以往恶性循环最终导致系统被迫下线。

如何避免呢？
1. 遵循一个标准，以搭建一个具有整洁，清晰代码结构的系统。
2. 在维护系统时，保持遵循这个标准。
3. 定期遵循这个标准来重构代码。

为了方便多人进行技术设计，开发及测试，需要一个标准化的代码架构

这个标准就是在领域驱动设计(DDD)思想下的具有最佳实践的代码分层分包架构模型COLA， 可解决代码架构降低耦合、高复用、分而治之。

## 什么是COLA

COLA为Clean Object-Oriented and Layered Architecture的英文缩写
中文意思`整洁面向对象分层架构`. 是来自阿里技术专家的[开源项目](https://github.com/alibaba/COLA)

该项目即提供代码分层分包架构思想，还提供代码的最佳实践及可服用组件。

下面重点说明COLA架构思想及如何使用
### COLA架构思想
#### 传统分层架构
![Img](./2828a142.png)
特点： 把系统分为表示层、业务逻辑层、数据访问层
考虑如下情况，就可以了解存在缺点：
1. 否有过困惑，代码到底应该放在哪个层，虽然定义了各层的职责，但是总有人不严格遵循层次的分界，比如业务逻辑写在了表示层，或者业务逻辑直接和数据访问绑定。
2. 对接消息中间件，调度访问及redis，这种一维的上下依赖层次就无法适应了。
深入思考后，传统分层架构容易时的系统代码变的臃肿，可扩展性和可伸缩性差。

#### 更好的架构模型COLA
先来看两张官方介绍图

1. 总体分层依赖流程图
![Img](./f3c61a69.png)
2. 明细分层分包一览图
![Img](./a3c9f354.png)
分层说明：
1. adapter层：面向外部的适配器层，相当传统controller层，针对不同外部端实现不同适配器，不涉任何业务逻辑，按外部端类型分包，有pc web, mobile, wap(慢慢被淘汰)。
2. app层：基于服务接口实现业务逻辑的应用层，需获取输入，组装上下文，参数校验，然后调用领域层或基础设施层完成数据更新，是先按照业务分包，再按照功能分包的，功能分包(executor-处理request请求, consumer-处理订阅消息, scheduler-处理调度请求)
3. client层：定义服务接口和dto，承接adapter层到app层，client层也可称为SDK层，可将打包JAR供其他模块或系统使用。
4. domain层：业务领域层封装了核心业务逻辑，实现领域服务（Domain Service）和领域对象（Domain Entity），为App层提供领域实体和领域服务，先按照业务分包，再按照功能分包的(domainservice-领域服务层，gateway-基础实施层网关服务层， 领域实体)
5. infrastructure层：基础设施层负责第三方底层技术处理，比如数据库的CRUD、搜索引擎、文件系统、分布式服务的RPC等。domain层的定义gateway也在这里实现，才能被上面的App层或domain层调用， 按gatewayimpl。config, mapper等方式分层，不同基础设施，不同实现方式，分层会有所不同。

COLA模型特点： 代码依赖关系更加松散，代码模块化，更具有复用性，可分而治之。

COLA分为两个部分，
1. COLA架构最佳实践方法
2. COLA通用组件，可直接依赖使用

其次，还有一个官方的表格，介绍了COLA中每个层的命名和含义：
| 层次       | 包名        | 功能                            | 必选 |
| ---------- | ----------- | ------------------------------- | ---- |
| Adapter层  | web         | 处理页面请求的Controller        | 否   |
| Adapter层  | wireless    | 处理无线端的适配                | 否   |
| Adapter层  | wap         | 处理wap端的适配                 | 否   |
| App层      | executor    | 处理request，包括command和query | 是   |
| App层      | consumer    | 处理外部message                 | 否   |
| App层      | scheduler   | 处理定时任务                    | 否   |
| Domain层   | model       | 领域模型                        | 否   |
| Domain层   | ability     | 领域能力，包括DomainService     | 否   |
| Domain层   | gateway     | 领域网关，解耦利器              | 是   |
| Infra层    | gatewayimpl | 网关实现                        | 是   |
| Infra层    | mapper      | ibatis数据库映射                | 否   |
| Infra层    | config      | 配置信息                        | 否   |
| Client SDK | api         | 服务对外透出的API               | 是   |
| Client SDK | dto         | 服务对外的DTO                   | 是   |

## 如何获取阿里COLA

官网地址： https://github.com/alibaba/COLA

关键说明： 
1. 不推荐使用：https://start.aliyun.com/bootstrap.html 生成cola应用， 原因不是最新的cola应用代码
2. 不推荐使用：mvn archetype:generate来获取cola模版的应用代码，原因也有可能不是最新cola代码
3. 推荐使用原来来安装和编译

### 阿里COLA4x源码编译和部署过程 
[阿里开源COLA 4.0源码编译和部署过程](https://www.cnblogs.com/rickie/p/15415016.html)

1. 首先获取cola源码：https://github.com/alibaba/COLA
2. 构建maven的archetype模版项目
   运行命令：mvn archetype:create-from-project， target目录下生成cola-framework-archetype-service-archetype-4.0.1.jar

3. 将模版项目安装到maven
   进入到target/generated-sources/archetype目录下
   运行mvn install，将这个jar安装到本地仓库，即可用archetype来生成项目。
   执行mvn deploy，安装到中央仓库，就可以共享给别来生成项目。
4. 添加自定义的archetype模版
   在IDEA中添加上述编译生成的COLA项目模板，file -> new project->maven -> create from archetype
   选择cola-framework-archetype-service-archetype-4.0.1.jar
   填写groupid、artifactid、version 信息
   `<groupId>com.alibaba.cola</groupId>
   <artifactId>cola-framework-archetype-service</artifactId>
   <version>4.0.1</version>`

### IDE来构建cola模版项目

## COLA通用组件


## 参考资料

[如何保证同事的代码不会腐烂？一文带你了解 Alibaba COLA 架构](
https://www.cnblogs.com/rude3knife/p/cola-architecture.html?share_token=a7c45034-e7bf-4574-8b1b-31c4377167f3)

## 下一步行动