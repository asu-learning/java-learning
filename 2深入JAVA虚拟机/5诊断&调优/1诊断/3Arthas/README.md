快速入门

官方文档：https://arthas.aliyun.com/doc/

linux和mac下安装

```bash
wget https://arthas.aliyun.com/arthas-boot.jar
curl -O https://arthas.aliyun.com/arthas-boot.jar

# 启动服务
java -jar arthas-boot.jar
如果下载速度比较慢，可以使用aliyun的镜像：

java -jar arthas-boot.jar --repo-mirror aliyun --use-http
java -jar arthas-boot.jar 
java -jar arthas-boot.jar pid 或 processName
```

docker下安装

```bash
docker exec -it  ribo-tmall-vip-9082 /bin/bash -c "wget https://arthas.aliyun.com/arthas-boot.jar && java -jar arthas-boot.jar"

# 若查找不到java进程ID， 手动查找
docker exec -it  ribo-tmall-vip-9082 /bin/bash -c "ps aux|grep java"
docker exec -it  ribo-tmall-vip-9082 /bin/bash -c "wget https://arthas.aliyun.com/arthas-boot.jar"
docker exec -it  ribo-tmall-vip-9082 /bin/bash -c "java -jar arthas-boot.jar"


```

关键点说明

```
arthas使用jdk工具包来实现监控诊断，因此java应用程序的运行环境必须支持安装JDK

若使用openJDK，或下docker镜像，则手动安装JDK，以确保arthas正常使用
FROM openjdk:8-jdk 或 FROM openjdk:8-jdk-alpine
参考： https://arthas.aliyun.com/doc/docker.html
```

快速入门

https://arthas.aliyun.com/doc/quick-start.html

进阶使用

https://arthas.aliyun.com/doc/advanced-use.html

实例分析





![](/Users/asushiye/Library/Application%20Support/marktext/images/2022-01-14-11-43-43-image.png)
