# 2Class

## 简述
类是一种用户自定义的类型，包含数据成员和成员函数。

内部类是定义在另一个类内部的类，它具有以下几个特点：
1）内部类中的变量和方法不能声明为静态的；
2）内部类的实例化需要先创建外部类的对象；
3）内部类可以引用外部类的静态或者非静态属性及方法。

设计内部类目的：把一组相近的功能内聚到一个类中，同时起到对外屏蔽可见性的作用，从而减少对外暴露的接口，使得源码结构更简洁。

静态内部类，它是定义在另一个类内部的静态类，与非静态内部类相比，有以下不同：
1）静态内部类可以具有public、protected、默认和private四种访问权限，而非静态内部类只能具有public和protected两种访问权限，或者默认访问权限；
2）静态内部类不依赖于外部类的实例。因此，静态内部类常用于创建那些不需要访问外部类状态的工具方法。

```java
/**
 * 定义Class类，用于表示对象运行时的信息
 */
public final class Class<T> implements java.io.Serializable,
                              GenericDeclaration,
                              Type,
                              AnnotatedElement {
    
    private static native void registerNatives();
    static {
        registerNatives();
    }
    
    /**
    * 定义Class类，用于表示对象运行时的信息
    */
    public T newInstance()


    public native boolean isInstance(Object obj);

    public native boolean isInterface();
    public native boolean isArray();
    public boolean isAnnotation() {
        return (getModifiers() & ANNOTATION) != 0;
    }

    /**
    * 定义Class类，用于表示对象运行时的信息
    */
    public String getName() {
        String name = this.name;
        if (name == null)
            this.name = name = getName0();
        return name;
    }

    /**
    * 定义Class类，用于表示对象运行时的信息
    */
    public String getSimpleName() 


    public Package getPackage() {
        return Package.getPackage(this);
    }

    public Class<?>[] getInterfaces() {
        ReflectionData<T> rd = reflectionData();
        if (rd == null) {
            // no cloning required
            return getInterfaces0();
        } else {
            Class<?>[] interfaces = rd.interfaces;
            if (interfaces == null) {
                interfaces = getInterfaces0();
                rd.interfaces = interfaces;
            }
            // defensively copy before handing over to user code
            return interfaces.clone();
        }
    }

    
    /**
    * 获取某个对象的修饰符（modifiers）
    */
    public native int getModifiers();

    /**
    * 用于获取某个对象的类型名称
    */
    public String getTypeName()    

    @CallerSensitive
    public Field[] getFields() throws SecurityException {
        checkMemberAccess(Member.PUBLIC, Reflection.getCallerClass(), true);
        return copyFields(privateGetPublicFields(null));
    }

    /**
    * 返回类及其父类中所有的公共字段（public fields）。它包括公共、保护、默认（包）访问和私有字段，以及继承的公共字段
    */
    @CallerSensitive
    public Field getField(String name)
        throws NoSuchFieldException, SecurityException {
        checkMemberAccess(Member.PUBLIC, Reflection.getCallerClass(), true);
        Field field = getField0(name);
        if (field == null) {
            throw new NoSuchFieldException(name);
        }
        return field;
    }

    /**
    * 返回类中声明的所有字段，包括公共、保护、默认（包）访问和私有字段，但不包括继承的字段
    */
    @CallerSensitive
    public Field[] getDeclaredFields() throws SecurityException {
        checkMemberAccess(Member.DECLARED, Reflection.getCallerClass(), true);
        return copyFields(privateGetDeclaredFields(false));
    }

    /**
    * 返回类中声明的所有字段，包括公共、保护、默认（包）访问和私有字段，但不包括继承的字段
    */
    @CallerSensitive
    public Method[] getMethods() throws SecurityException {
        checkMemberAccess(Member.PUBLIC, Reflection.getCallerClass(), true);
        return copyMethods(privateGetPublicMethods());
    }

    @CallerSensitive
    public Method getMethod(String name, Class<?>... parameterTypes)
        throws NoSuchMethodException, SecurityException {
        checkMemberAccess(Member.PUBLIC, Reflection.getCallerClass(), true);
        Method method = getMethod0(name, parameterTypes, true);
        if (method == null) {
            throw new NoSuchMethodException(getName() + "." + name + argumentTypesToString(parameterTypes));
        }
        return method;
    }

    @CallerSensitive
    public Constructor<?>[] getConstructors() throws SecurityException {
        checkMemberAccess(Member.PUBLIC, Reflection.getCallerClass(), true);
        return copyConstructors(privateGetDeclaredConstructors(true));
    }

    public Annotation[] getAnnotations() {
        return AnnotationParser.toArray(annotationData().annotations);
    }

    @SuppressWarnings("unchecked")
    public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
        Objects.requireNonNull(annotationClass);

        return (A) annotationData().annotations.get(annotationClass);
    }

}
```