# 1Object


```java
/**
 * 它是 Java 语言中所有类的根类。
 */
public final class Object {

    /**
     * 注册本地方法（native methods）。本地方法是使用其他编程语言（如C或C++）编写的方法，通过JNI技术与Java代码进行交互。
     */
    private static native void registerNatives();
    
    /**
     * 这是静态代码块，当在类加载时触发执行，用于进行一些初始化操作或执行一些必要的设置
     * 注意： 并不是实例化对象时触发执行
     */
    static {
        registerNatives();
    }

    /**
     * 返回此对象的哈希码值。
     * asushiye解读： 
     * 通过哈希码值保证对象的唯一性
     * native 修饰符，表示本地方法，无需在java实现，当运行时，jvm会调用目标系统的本地库或API来实现方法

     扩展解读：
     在Java中，所有的对象都存储在堆内存中，而堆内存是由多个块组成的。
     每个块都有一个唯一的标识符，称为“桶”（bucket）。当一个对象被创建时，被添加到哈希表HashMap中时，并存储到对应的一个桶中。
     如果需要访问该对象，则可以使用哈希表来查找它所在的桶，并从该桶中获取对象的引用。
     
     一个桶可存放多个不同的对象，如果两个对象具有相同的哈希码，则它们被认为是相等的，因此它们将共享同一个桶。
     如果两个对象的哈希码不同，则它们将位于不同的桶中，支持各自的插入、删除和查找操作。

     */
    public native int hashCode();

    /**
     * 返回对象的字符串表示形式。
     * 默认情况下Object类的toString()方法返回一个字符串"java.lang.Object@该对象的哈希码的无符号十六进制值"
     * 这个默认的实现不能提供有用的信息来描述对象的内容，因此此方法支持重写以便返回更有意义的字符串信息
     */
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    /**
     * 返回对象的运行时类Class<?>， 由于Object可以任意的对象，因此用 ？表示未知的对象类型的Class
     * 通过Class为对象的元数据，可通过它获取类，接口，方法，注解，成员等信息
     * final方法，表示它不能被子类覆盖
     */
    public final native Class<?> getClass();

    /**
     * 用于比较两个对象是否相等
     * 默认下比较两个对象的引用是否相同。也就是说是不是引用了同一个内存地址，则它们被认为是相等的。
     * 这种实现方式通常不适用于大多数情况，因为它不能正确地比较两个对象的内容是否相等
     * 因此建议子类继续重写此方法
     */
    public boolean equals(Object obj) {
        return (this == obj);
    }
    /**
     * 克隆原对象，并返回一个新对象
     * 修饰符protected表示只有同一个包中的类或子类可以访问它。
     * 修饰符native表示它是由本地代码实现的，而不是由Java代码实现的
     * 
     * 如果想让子类支持克隆功能，则需要实现Cloneable接口，重写clone()方法，实现对象的所有字段复制到新对象
     */
    protected native Object clone() throws CloneNotSupportedException;

    /**
     * 用于唤醒一个在该对象上等待的线程， 当一个线程调用对象的wait()方法时，线程进入该对象的等待列表中并等待被唤醒。
     * 当另一个线程调用该对象的notify()方法时，它会从等待队列中唤醒一个线程，使其有机会继续执行。
     
     * 修饰符native表示它是由本地代码实现的，而不是由Java代码实现的
     * 修饰符final表示它不能被重写的
     * 
     * 特别重要是：
     * 如果多个线程同时调用同一个对象的notify()方法，可能会导致竞争冲突，产生不可预测的结果。
     * 为了避免这种情况，需要使用锁机制，排除竞争冲突，可以使用synchronized关键字或其他同步机制来确保只有一个线程可以访问该对象。
     */
    public final native void notify();

    /**
     * 唤醒所有在该对象上等待的线程
     */
    public final native void notifyAll();


    /**
     * 用于使当前线程进入等待状态，直到其他线程调用该对象的notify()方法或notifyAll()方法。
     * 参数timeout，表示等待的最长时间（以毫秒为单位）。
     * 如果在指定的时间内没有被唤醒，则该方法会抛出InterruptedException异常
     * 方法必须在同步块中使用，以确保线程安全
     */
    public final native void wait(long timeout) throws InterruptedException;

    /**
     * 用于在垃圾回收器回收对象之前执行一些清理操作
     * 当一个对象不再被引用时，垃圾回收器会自动回收该对象所占用的内存空间。
     * 然而在某些情况下，可能需要在对象被回收之前执行一些额外的清理操作，例如关闭文件句柄或释放其他资源。
     *· 在这种情况下，可以使用finalize()方法来实现这些操作。

     * 需要注意的是，由于finalize()方法的运行时间是不确定的，因此它不应该用于执行重要的清理操作。
     * 此外由于finalize()方法可能会引起性能问题，因此应该尽量避免使用它。
     * 相反，应该使用其他机制来管理资源的释放，例如try-with-resources语句或显式地关闭资源。

     * 另外，从Java 9开始，finalize()方法已经被标记为过时（deprecated），并且不建议使用。
     * 取而代之的是，可以使用java.lang.ref.Cleaner或java.lang.ref.PhantomReference接口来替代finalize()方法
     */

    protected void finalize() throws Throwable { }
}

```